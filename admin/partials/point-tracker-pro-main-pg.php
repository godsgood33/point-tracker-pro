<?php

/**
 * File: point-tracker-pro-main-pg.php
 * Author: Ryan Prather
 * Purpose: To display the main admin page
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppointtracker.com
 * @since      1.0.0
 *
 * @package    Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/admin/partials
 */
global $wpdb;

if (! current_user_can('manage_options')) {
    wp_die("You do not have permissions to do this", "You Dirty Rat!", array(
        'response' => 301
    ));
}

$query = "SELECT * FROM {$wpdb->prefix}pt_challenges";
$challenges = $wpdb->get_results($query) or [];

?>

<div id='waiting'></div>
<div id='loading'></div>

<h2>Point Tracker</h2>
<div id='msg'></div>
<input type='hidden' id='_wpnonce'
    value='<?php print wp_create_nonce('ptp-delete-challenge'); ?>' />
<input type='button' id='save-challenge' value='Save' />&nbsp;&nbsp;
<input type='button' id='delete-challenge' value='Delete' />&nbsp;&nbsp;
<input type='button' id='remove-winner' value='Remove Winner' />
<?php if(get_option('ptp-mailserver-host')) { ?>
&nbsp;&nbsp;
<input type='button' id='email-participant-list'
    value='Email Participant List' style='display: none;' />
&nbsp;&nbsp;
<input type='button' id='email-participant-log'
    value='Email Participant Log' style='display: none;' />
<?php } else { ?>
&nbsp;&nbsp;
<span id='email-participant-list' style='display:none;background-color:yellow;border:solid 1px black;'>You need to setup your email in Settings -> PT Settings</span>
<?php } ?>
<br />
<br />
Challenge Name:
<select id='challenge'>
    <option value=''>-- Select Challenge --</option>
<?php
foreach ($challenges as $chal) {
    $name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);
    print "<option value='{$chal->id}'>{$name}</option>";
}
?>
</select>

<div id='winner'></div>
<div id='tooltip'></div>

<div>
    <input type='text' id='name' class='tooltip-field'
        placeholder='Name...' data-title='A name for this challenge' />
</div>
<div>
    <input type='text' id='start-date' placeholder='Start Date...'
        class='tooltip-field' data-title='Start date for the challenge. Based on timezone: <?php print get_option('timezone_string'); ?>' />
</div>
<div>
    <input type='text' id='end-date' placeholder='End Date...'
        class='tooltip-field' data-title='End date for the challenge. Based on timezone: <?php print get_option('timezone_string'); ?>' />
</div>
<div class='tooltip-field'
    data-title='Do you want people to add themselves to the challenge?'>
    <input type='checkbox' id='private' /> <label for='private'>Private Challenge?</label>
</div>
<div class='tooltip-field'
    data-title='Do you want to approve requests to join the challenge (requires account)'>
    <input type='checkbox' id='approval' />
    <label for='approval'>Approval Required?</label>
</div>
<div class='tooltip-field'
    data-title='Do you want to allow users to backdate their entries'>
    <input type='checkbox' id='backdating' />
	<label for='backdating'>Backdating?</label>
</div>
<div class='tooltip-field'
    data-title='Do you want to make a leader board visible to the users (one is always available to the admin)?'>
    <input type='checkbox' id='leader-board' />
    <label for='leader-board'>Leader Board?</label> &nbsp;&nbsp;
    <select id='leader-count' disabled='disabled' class='tooltip-field'
        data-title='How many participants do you want to display on the leader board?'>
        <option value=''>All</option>
        <option value='1'>1</option>
        <option value='3'>3</option>
        <option value='5'>5</option>
        <option value='10'>10</option>
        <option value='25'>25</option>
        <option value='50'>50</option>
        <option value='100'>100</option>
    </select>
</div>

<div>
    Link:&nbsp;&nbsp; <span id='link' class='tooltip-field'
        data-title='Link to the challenge. Copy/paste this when you are ready for people to start joining.'></span>&nbsp;&nbsp;
    <span id='challenge-page'></span>
	<input type='button' id='create-page' value='Create Page' data-title='Click to create a dedicated page' />
</div>
<div>
    Description: <br />
    <textarea id='desc' rows='5' cols='100' class='tooltip-field'
        data-title='A long description for what the challenge seeks to accomplish, and what, if any, prize will be rewarded'></textarea>
</div>

<div id='leader-list-container'>
    <input type='checkbox' id='leader-list' class='tooltip-field'
        data-title='Do you want to allow users to select a group leader they are under' />
    <label for='leader-list'>Group?</label>

    <div id='leader-list-box'>
        <i class='fas fa-user-plus' id='add-leader'
            data-title='Click to add a new leader'></i>
        <table id='leader-list-table' style='width: 650px;'></table>

        <div id="dialog-form" data-title="Add new leader">
            <p class="validateTips">All form fields are required.</p>

            <form>
                <fieldset>
                    <input type="text" id="leader-member-id"
                        placeholder="Member ID..." inputmode='numeric'
                        pattern='[0-9]*'
                        class="text ui-widget-content ui-corner-all" />
                    <input type="text" id="leader-name"
                        placeholder="Name..."
                        class="text ui-widget-content ui-corner-all" />
                    <input type="email" id="leader-email"
                        placeholder="Email..."
                        class="text ui-widget-content ui-corner-all" />

                    <!-- Allow form submission with keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1"
                        style="position: absolute; top: -1000px" />
                </fieldset>
            </form>
        </div>
    </div>
</div>
<br />
Activity Count:&nbsp;&nbsp;
<span id='act-count'></span>
<br />
Participant Count:&nbsp;&nbsp;
<span id='part-count'></span>
