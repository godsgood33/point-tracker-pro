<?php
if (! current_user_can('manage_options')) {
    wp_die("You do not have permissions to do this", "You Dirty Rat!", [
        'response' => 301
    ]);
}

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

if ($action == 'Save Settings') {
    if (! check_ajax_referer('ptp-update-options', '_wpnonce', false)) {
        print "Unable to verify permissions";
        wp_die();
    }

    $req_login = (boolean) filter_input(INPUT_POST, 'require-login', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $email_new = (boolean) filter_input(INPUT_POST, 'email-new', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $admin_summary = (boolean) filter_input(INPUT_POST, 'admin-summary-email', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    update_option('ptp-require-login', ($req_login ? 1 : 0));
    update_option('ptp-admin-summary-email', ($admin_summary ? 1 : 0));
    update_option('ptp-email-new-participants', ($email_new ? 1 : 0));

    $host = filter_input(INPUT_POST, 'mail-host', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $port = filter_input(INPUT_POST, 'mail-port', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $user = filter_input(INPUT_POST, 'mail-user', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $pwd = filter_input(INPUT_POST, 'mail-pwd', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $smtp_auth = (boolean) filter_input(INPUT_POST, 'smtp-auth', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

    update_option('ptp-mailserver-host', $host);
    update_option('ptp-mailserver-port', $port);
    update_option('ptp-mailserver-user', $user);
    update_option('ptp-mailserver-pwd', $pwd);
    update_option('ptp-mailserver-require-smtp-auth', ($smtp_auth ? 1 : 0));

    print "Saved Settings<br />";
} elseif($action == 'Save') {
    $random_key = filter_input(INPUT_POST, 'ptp-random-api-key', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    update_option('ptp-random-api-key', $random_key);
}
?>

<h2>Point Tracker Settings</h2>

<?php
$status = get_option('ptp-license-status', null);
if ($status == 'valid') {
    ?>
<div>
    <form method='post' action='#'>
        <input type='hidden' name='_wpnonce'
            value='<?php print wp_create_nonce('ptp-update-options'); ?>' />
        <div class='notice notice-warning'>
			<?php print __('Not requiring a login opens challenge participants to potential unauthorized activity deletions', 'point-tracker-pro'); ?>
		</div>
        <div>
            <input type='checkbox' name='require-login'
                id='require-login' value='1'
                <?php print (get_option('ptp-require-login', 0) ? "checked" : ''); ?> />&nbsp;&nbsp;
            <label for='require-login'>Login Required?</label>
        </div>

        <!--
        <div>
            <input type='checkbox' name='admin-summary-email'
                id='admin-summary-email' value='1'
                <?php print (get_option('ptp-admin-summary-email', 0) ? "checked" : ''); ?> />&nbsp;&nbsp;
            <label for='admin-summary-email'>Send Admin a Summary Email?</label>
        </div>
        -->

        <div>
            <input type='checkbox' name='email-new'
                id='email-new-participants' value='1'
                <?php print (get_option('ptp-email-new-participants', 0) ? 'checked' : ''); ?> />&nbsp;&nbsp;
            <label for='email-new-participants'>Email New Participant?</label>
        </div>

        <hr />

        <div>
            <input type='text' name='mail-host' placeholder='Mailserver Host...'
                <?php print (get_option('ptp-mailserver-host') ? "value='" . get_option('ptp-mailserver-host') . "'" : null); ?> />
        </div>

        <div>
            <input type='text' name='mail-port' placeholder='Mailserver Port...'
                <?php print (get_option('ptp-mailserver-port') ? "value='" . get_option('ptp-mailserver-port') . "'" : null); ?> />
        </div>

        <div>
            <input type='text' name='mail-user' placeholder='Mailserver User...'
                <?php print (get_option('ptp-mailserver-user') ? "value='" . get_option('ptp-mailserver-user') . "'" : null); ?> />
        </div>

        <div>
            <input type='password' name='mail-pwd' placeholder='Mailserver Password...'
                <?php print (get_option('ptp-mailserver-pwd') ? "value='" . get_option('ptp-mailserver-pwd') . "'" : null); ?> />
        </div>

        <div>
            <input type='checkbox' name='smtp-auth' id='require-smtp-auth' value='1'
                <?php print (get_option('ptp-mailserver-require-smtp-auth') ? "checked" : null); ?> />&nbsp;&nbsp;
            <label for='require-smtp-auth'>Mailserver Requires SMTP Auth? </label>
        </div>

        <hr />

        <input type='submit' name='action' value='Save Settings' />
    </form>
    
    <form method='post' action='#'>
        <ol>
            <li>Goto <a href="https://api.random.org/json-rpc/1/" target="_blank">https://api.random.org/json-rpc/1/</a></li>
            <li>Click "Get a Beta Key" in the top right corner of the page</li>
            <li>Enter your email address in the box and click "Send Beta Key"</li>
            <li>Open the email that was sent to you by Random.org and copy the long random looking string to the box below and click "Save"</li>
        </ol>
        
        <div title='Limits are per day'>
        	Random.org API requests left: <?php print get_option('ptp-random-requests', 0); ?><br />
        	Random.org API bytes lefts: <?php print get_option('ptp-random-bits', 0); ?>
        </div>
        
        <label for="random-apiKey">API Key:</label>
        <input type="text" id="ptp-random-api-key" name='ptp-random-api-key' placeholder="Random.org API Key" value="<?php print get_option('ptp-random-api-key', null); ?>" /><br />
        
        <input type="submit" id="action" name='action' value="Save" />
    </form>
<?php }
