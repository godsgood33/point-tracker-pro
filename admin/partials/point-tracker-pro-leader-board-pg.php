<?php
/**
 * File: point-tracker-pro-leader-board-pg.php
 * Author: Ryan Prather
 * Purpose: To display the admin leaderboard
 */
global $wpdb;

if (! current_user_can('manage_options')) {
    wp_die('You do not have permissions to do this', "You Dirty Rat!", array(
        'response' => 301
    ));
}

$query = "SELECT * FROM {$wpdb->prefix}pt_challenges";
$challenges = $wpdb->get_results($query);

?>

<h2>Leader Board</h2>

<div id='msg'></div>
<div id='waiting'></div>
<div id='loading'></div>

Challenge Name:
<select id='challenge-leader-board'>
    <option value=''>-- Select Challenge --</option>
  <?php foreach($challenges as $chal): print "<option value='{$chal->id}'>{$chal->name}</option>"; endforeach; ?>
</select>
<br />

<div id='left-half'>
    <table id='leader-board-table'>
    </table>
</div>

<div id='right-half'>
    <table id='activity-table'>
    </table>
</div>