<?php
/**
 * File: point-tracker-pro-random-selector-pg.php
 * Author: Ryan Prather
 * Purpose: To allow the admin to select a random winner
 */

global $wpdb;

if (! current_user_can('manage_options')) {
    wp_die('You do not have permissions to do this', "You Dirty Rat!", [
        'response' => 301
    ]);
} elseif (!get_option('ptp-random-api-key', null)) {
    wp_die('You have not retrieved a Random.org API key and saved it in the Settings -> Point Tracker', 'Try again', [
        'response' => 301
    ]);
}

$query = "SELECT * FROM {$wpdb->prefix}pt_challenges";
$challenges = $wpdb->get_results($query) or [];
?>

<h2>Random Name Drawing</h2>

<div id='msg'></div>
<div id='waiting'></div>
<div id='loading'></div>

<select id='random-selector-ptp-challenge'>
<option value=''>-- Select Challenge --</option>
<?php
foreach ($challenges as $chal) {
    $name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);
    print "<option value='{$chal->id}'>{$name}</option>";
}
?>
</select>&nbsp;&nbsp;

<select id='random-selector-ptp-activity'>
	<option value=''>-- Filter Activity --</option>
</select>&nbsp;&nbsp;

<select id='random-selector-leader-list'>
	<option value=''>-- Filter Leader --</option>
</select>&nbsp;&nbsp;

<label for='point-min'>Point Min: </label>
<input type='number' id='point-min' min='0' value='0' />&nbsp;&nbsp;

<label for='entry-per-point'>Assign one entry/point for each participant? </label>
<input type='checkbox' id='entry-per-point' value='1' title='This will give each participant one drawing entry/point they have' />&nbsp;&nbsp;

<input type='button' id='select-random-winner' value='Select Winner' /><br />

<table id='random-selection-table'></table>
