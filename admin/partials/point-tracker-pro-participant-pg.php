<?php
/**
 * File: point-tracker-pro-participant-pg.php
 * Author: Ryan Prather
 * Purpose: To display to the admins the participant list
 */
global $wpdb;

if (! current_user_can('manage_options')) {
    wp_die('You do not have permissions to do this', "You Dirty Rat!", array(
        'response' => 301
    ));
}

$query = "SELECT * FROM {$wpdb->prefix}pt_challenges";
$challenges = $wpdb->get_results($query) or [];

if (isset($_FILES['participant-upload'])) {
    $csv = str_replace(' ', '-', $_FILES['participant-upload']['name']);
    $ft = wp_check_filetype($csv);
    if(in_array($ft['type'], ['text/csv', 'application/vnd.ms-excel'])) {
        if(!is_uploaded_file($_FILES['participant-upload']['tmp_name'])) {
            error_log("Failed to upload {$_FILES['participant-upload']['name']}");
            die();
        }
        ptp_participant_upload($_FILES['participant-upload']['tmp_name']);
    }
}

$not = (get_option('ptp-email-new-participant', 0) ? "" : " NOT");

?>

<h2>Participant List</h2>

<div id='msg'></div>
<div id='waiting'></div>
<div id='loading'></div>

<input type='hidden' id='_wpnonce'
    value='<?php print wp_create_nonce('ptp-delete-participant'); ?>' />

Challenge Name:
<select id='challenge_participants'>
    <option value=''>-- Select Challenge --</option>
<?php
foreach ($challenges as $chal) {
    $name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);
    print "<option value='{$chal->id}'>{$name}</option>";
}
?>
</select>

<div id='tooltip'></div>

<input type='button' id='clear-participants' value='Clear Participants' />
&nbsp;&nbsp;
<a href='javascript:void(0);' id='upload-participant-file-link'>Upload
    Participant File</a>
&nbsp;&nbsp;
<a href='javascript:void(0);' id='add-participant-link'>Add Participant</a>

<div id='admin-add-participant'>
    <input type='text' id='member-id' placeholder='Member ID...'
        inputmode='numeric' pattern='[0-9]*' /><br />
    <input type='text' id='user-name' placeholder='Name...' /><br />
    <input type='email' id='user-email' placeholder='Email...' /><br />
    <select id='select-leader'></select>
    <input type='button' id='add-participant' value='Add Participant' />
</div>

<table id='participant-table' class='display'></table>

<div id='upload-container' style='display: none;'>
    Select a CSV file to upload a list of your participants and
    automatically add them to the challenge<br />
    Click <a href='javascript:void(0);' id='csv-sample-link'>Here</a> to see
    a sample of what the CSV file should look like.<br />
    This WILL<?php print $not; ?> email those in the file a notification they have been added.<br />
    <form method='post' enctype='multipart/form-data'>
        <input type='hidden' id='upload-chal-id' name='chal-id' />
		<input type='file' name='participant-upload' />
        <?php print submit_button("Upload"); ?>
    </form>
</div>

<div id='csv-sample' style='display: none;'>
    Leader ID optional and only necessary if your challenge uses leaders
    and you want to auto-assign the participant to a leader. Use the same ID used in creating the leaders
    <pre>ID,Name,Email,Leader ID</pre>
</div>