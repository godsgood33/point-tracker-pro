<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       https://bitbucket.org/godsgood33
 * @since      1.0.0
 *
 * @package    Point_Tracker_Pro
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

delete_option('ptp-require-login');
delete_option('ptp-email-new-participants');
delete_option('ptp-admin-summary-email');
delete_option('ptp-mailserver-host');
delete_option('ptp-mailserver-port');
delete_option('ptp-mailserver-user');
delete_option('ptp-mailserver-pwd');
delete_option('ptp-mailserver-require-smtp-auth');

update_option('ptp-license-key', '');
update_option('ptp-license-status', 'inactive');

$chal = get_page_by_title('Challenge');
wp_delete_post($chal->ID);
$chal_list = get_page_by_title('Challenge List');
wp_delete_post($chal_list->ID);
$lb = get_page_by_title('Leader Board');
wp_delete_post($lb->ID);
$lb_list = get_page_by_title('Leader List');
wp_delete_post($lb_list->ID);
$act = get_page_by_title('My Activity');
wp_delete_post($act->ID);

// drop tables, views, and SQL functions
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pt_challenges");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pt_activities");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pt_participants");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pt_log");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pt_leader_list");
$wpdb->query("DROP VIEW IF EXISTS {$wpdb->prefix}point_totals");
$wpdb->query("DROP VIEW IF EXISTS {$wpdb->prefix}leader_board");
$wpdb->query("DROP VIEW IF EXISTS {$wpdb->prefix}user_activity");
$wpdb->query("DROP FUNCTION IF EXISTS get_pt_activity_id");
$wpdb->query("DROP FUNCTION IF EXISTS get_pt_challenge_id");
$wpdb->query("DROP FUNCTION IF EXISTS get_pt_user_id");
