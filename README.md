# Point Tracker Pro

This plugin is essentially a form builder, but for challenges and contests.  After installation, a site admin needs to create a challenge inserting a name, start and end dates, a description, and decide if they want to require users to be approved (which requires site account).  The admin can also allow participants to backdate their activity (say they did it on a date other than when they are actually entering) and gives admins the ability to make a public leader board available with challenge leader counts of 1, 3, 5, 10, 25, 50, 100, or all

Once saved, the admin then navigates to the Point Tracker -> Activities admin menu and after selecting the challenge they just created, they add activities they want the participants to complete.  The following fields are allowed (* = required):

- *Type (checkbox, text, radio, number)
- *Name (short name not displayed)
- *Point value (the amount of points to be awarded each time this activity is done)
- Max allowed (the maximum amount of points allowed to be collected during this challenge)
- *Question (the short question to ask the user)
- *Description (a longer description of the question and requirements for it's completion)
- *Order (the order in which the question will appear to the user)
- *Label (only available in checkbox and radio button questions, but a comma delimited list of options that will be presented to the user)
- Min/Max (only available in numeric and text questions, but the minimum and maximum amount allowed)
- Start/End (dates that limit the visibility, inclusively, of an activity)

After creating all activities, you can go back to the main Point Tracker admin page and copy the link and send or post it to those that want to participate.  Once in the challenge (depending on your previous selections), participants will be able to independently save activities they've accomplished.  So for example, one of your numeric activities is "Make a phone call" and you award 5 points for doing it.  When the participant goes to the page to complete their entry, they might say they made 5 phone calls and put 5 in the entry box and click "Save" for that activity.  The system will do the math and calculate they earned 25 points for completing that task.  This will then show on the admin Dashboard under Point Tracker -> Participants page.  Which reflects a list of all participants and their point tallys.

Also, as an option on Point Tracker -> Participants you can now upload a CSV file with all your participants in it to automatically invite them to the challenge.  The CSV file must contain the following headers and columns.

- ID
- Name
- Email
- Leader ID (optional)

The Point Tracker -> Log page shows a raw log of all entries that have been made and the ability for the admin to delete an entry and allow the participant to reenter or forfeit those points.  They will automaticaly be removed from any total point calculations.

Throughout the challenge the admin can go to the Point Tracker -> Leader Board page to see the status of everybody in the challenge.  This page is always availabe regardless of challenge selection.  When selecting a challenge the admin will see two tables.  One for all the participants and their total points.  The second table on the right side is a list of all the activities that people have entered points on and the current point leader of that activity.  If two participants tie in their point count, it defaults to showing the one that got to their points chronologically first.

Once the challenge is complete the admin can go to the Point Tracker -> Leader Board page to see the person with the most points.  As part of your challenge process, I recommend that you validate the entries by the participants to make sure things were entered and recorded correctly.  The way you would validate the points will vary depending on how your challenge is structured and what activities you require your particpants to complete.

There is a global options under Settings -> PT Settings for the following:

- Requiring the participants be logged into an account before they are allowed to access any of the challenge pages
- Sending the site admin a summary email (right now just lets the admin know of new participants in challenges that require approval)
- Sending new participants an email notifying them they have been approved to participate in a challenge

