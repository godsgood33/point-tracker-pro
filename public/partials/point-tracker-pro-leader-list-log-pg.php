<?php
/**
 * File: point-tracker-leader-list-log-pg.php
 * Author: Ryan Prather
 * Purpose: To display the leader board for public users
 */
global $wpdb;
$chal_link = filter_input(INPUT_GET, 'chal', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
if (! $chal_link) {
    wp_die("You need to select a challenge to get");
}

if(!is_admin()) {
$chal = Point_Tracker_Pro::init($chal_link);
?>

<div id='msg'></div>
<div id='loading'></div>
<div id='waiting'></div>

<div id='left-half'>
	<input type='hidden' id='chal-id' value='<?php print $chal->id; ?>' />
	<input type='text' id='leader-id' placeholder='Leader ID...'
		title='Enter your member ID EXACTLY as you first entered it' /><br />
	<input type='text' id='email' placeholder='Email...'
		title='Enter your email' /><br />
	<input type='button' id='leader-get-activity' value='Get My Log' />&nbsp;&nbsp;
	<div id='tp'>Total Points:
		<span id='total-points'></span>
	</div>
	<table id='my-activity-table'></table>
</div>

<?php } ?>
