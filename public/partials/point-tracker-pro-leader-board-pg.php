<?php
/**
 * File: point-tracker-leader-board.php
 * Author: Ryan Prather
 * Purpose: To display the leader board for public users
 */
global $wpdb;

$chal_link = filter_input(INPUT_GET, 'chal', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

if(!is_admin()) {
$chal = Point_Tracker_Pro::init($chal_link);
if(!$chal->leader_board) {
    wp_die("The challenge coordinator has selected not to allow for a leader board in this challenge");
}

if($chal->use_leader) {
    $query = "SELECT * FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d";
    $chal->leader_list = $wpdb->get_results($wpdb->prepare($query, $chal->id));
}

if (! (boolean) get_option('ptp-require-login', 0)) {
    $wpdb->query($wpdb->prepare("SET @challenge_id=%d", $chal->id));
    $lb = $wpdb->get_results("SELECT member_id,participant_name,SUM(total_points) as 'total'
FROM {$wpdb->prefix}leader_board
GROUP BY participant_name
ORDER BY `total` DESC " .
(is_numeric($chal->leader_count) ? "LIMIT {$chal->leader_count}" : ""));

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_activities WHERE `challenge_id`=%d", $chal->id);
    $activities = $wpdb->get_results($query);
    ?>

<input type='hidden' id='chal-link' value='<?php print $chal_link; ?>' />
<?php
if ($chal->use_leader) {
        ?>

	<select id='leader'>
		<option value=''>-- SELECT Leader --</option>
		<?php foreach($chal->leader_list as $ll) : print "<option value='{$ll->leader_id}'>{$ll->leader_name}</option>"; endforeach; ?>
	</select>
<?php } ?>
<div id='left-half'>
	<table id='leader-board-table'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Points</th>
			</tr>
		</thead>
		<tbody id='leader-board-body'>
  <?php
    if (! $chal->use_leader) {
        foreach ($lb as $leader) {
            $ln = html_entity_decode($leader->participant_name, ENT_QUOTES | ENT_HTML5);
            print <<<EOR
<tr>
    <td>{$ln}</td>
    <td>{$leader->total}</td>
</tr>
EOR;
        }
    }
    ?>
    	</tbody>
	</table>

</div>

<div id='right-half'>
	<table id='activity-table'>
		<thead>
			<tr>
				<th>Question</th>
				<th>Name</th>
				<th>High Score</th>
			</tr>
		</thead>
		<tbody id='activity-body'>
  <?php
    if (! $chal->use_leader) {
        foreach ($activities as $act) {
            $query = $wpdb->prepare("SELECT question,participant_name,SUM(total_points) as 'high_score'
FROM {$wpdb->prefix}leader_board
WHERE activity_id = %d
GROUP BY participant_name
ORDER BY total_points DESC, log_date
LIMIT 1", $act->id);
            $hs = $wpdb->get_row($query);

            if ($hs) {
                $question = html_entity_decode($hs->question, ENT_QUOTES | ENT_HTML5);
                $pn = html_entity_decode($hs->participant_name, ENT_QUOTES | ENT_HTML5);
                print <<<EOR
<tr>
    <td>{$question}</td>
    <td>{$pn}</td>
    <td>{$hs->high_score}</td>
</tr>
EOR;
            }
        }
    }
    ?>
  		</tbody>
	</table>
</div>
<?php
} elseif (is_user_logged_in()) {
    $lb = $wpdb->get_results("SELECT participant_name,SUM(total_points) as 'total'
FROM {$wpdb->prefix}leader_board
GROUP BY participant_name
ORDER BY `total` DESC" .
(is_numeric($chal->leader_count) ? " LIMIT {$chal->leader_count}" : ""));

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_activities WHERE `challenge_id`=%d", $chal->id);
    $activities = $wpdb->get_results($query);

    ?>

<div id='left-half'>
	<table id='leader-board-table'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Points</th>
			</tr>
		</thead>
		<tbody id='leader-board-body'>
<?php
    foreach ($lb as $leader) {
        $ln = html_entity_decode($leader->participant_name, ENT_QUOTES | ENT_HTML5);
        print <<<EOR
<tr>
    <td>{$ln}</td>
    <td>{$leader->total}</td>
</tr>
EOR;
    }
    ?>
  	</tbody>
	</table>
</div>

<div id='right-half'>
	<table id='activity-table'>
		<thead>
			<tr>
				<th>Activity</th>
				<th>Name</th>
				<th>High Score</th>
			</tr>
		</thead>
		<tbody id='activity-body'>
<?php
    foreach ($activities as $act) {
        $query = $wpdb->prepare("SELECT name,participant_name,SUM(total_points) as 'high_score'
FROM {$wpdb->prefix}leader_board
WHERE activity_id = %d
GROUP BY participant_name
ORDER BY total_points DESC, log_date
LIMIT 1", $act->id);
        $hs = $wpdb->get_row($query);

        if ($hs) {
            $name = html_entity_decode($hs->name, ENT_QUOTES | ENT_HTML5);
            $pn = html_entity_decode($hs->participant_name, ENT_QUOTES | ENT_HTML5);
            print <<<EOR
<tr>
    <td>{$name}</td>
    <td>{$pn}</td>
    <td>{$hs->high_score}</td>
</tr>
EOR;
        }
    }
    ?>
		</tbody>
	</table>
</div>
<?php
} else {
    print "You are not logged in";
}
}
?>

<div id='loading'></div>
<div id='waiting'></div>
