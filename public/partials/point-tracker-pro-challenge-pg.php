<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://wppointtracker.com
 * @since      1.0.0
 *
 * @package    Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/public/partials
 */
global $wpdb;

$chal_link = filter_input(INPUT_GET, 'chal', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

if(!$chal_link) {
    $chal_link = filter_var(Point_Tracker_Pro_Public::$chal, FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
}
if(!is_admin()) {
    $chal = Point_Tracker_Pro::init($chal_link);
    $act_page = get_page_by_title("My Activity");
    
    $lb_page = get_page_by_title("Leader Board");
    $now = new DateTime("now", new DateTimeZone(get_option("timezone_string")));
    $use_leader_board = (boolean) $chal->leader_board;
    
    $query = "SELECT *
    FROM {$wpdb->prefix}pt_activities
    WHERE
        challenge_id = %d AND
        (start_dt IS NULL OR start_dt <= %s) AND
        (end_dt IS NULL OR end_dt >= %s)
    ORDER BY `order`";
    $chal->activities = $wpdb->get_results($wpdb->prepare($query, $chal->id, $now->format("Y-m-d"), $now->format("Y-m-d")));
    $chal->name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);
    $chal->desc = nl2br(html_entity_decode(stripcslashes($chal->desc), ENT_QUOTES | ENT_HTML5));
    $groups = [];
    $grouped_activities = [];
    
    if(!$chal->activities) {
        wp_die("There are no activities in this challenge");
    }
    else {
        $query = "SELECT DISTINCT(`group`) AS 'g'
    FROM {$wpdb->prefix}pt_activities
    WHERE
        challenge_id = %d AND
        `group` IS NOT NULL AND
        `group` != ''";
        $groups = $wpdb->get_results($wpdb->prepare($query, $chal->id));
    }
    
    $part = null;
    if (is_user_logged_in()) {
        $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_participants WHERE user_id = %d", get_current_user_id());
        $part = $wpdb->get_row($query);
    }
    
    if($chal->winner) {
        $winner = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d AND user_id = %d", $chal->id, $chal->winner));
        
        print "<div id='winner'>{$winner} is the winner!</div>";
    } else {
        $query = "SELECT *
        FROM {$wpdb->prefix}pt_leader_list
        WHERE challenge_id = %d
        ORDER BY leader_name";
        $chal->leader_list = $wpdb->get_results($wpdb->prepare($query, $chal->id));
?>

<div id='msg'></div>
<div id='waiting'></div>
<div id='loading'></div>
<input type='hidden' id='chal-link'
    value='<?php print $chal->short_link; ?>' />

<h2><?php print $chal->name; ?></h2>
<small><?php print $chal->desc; ?></small>
<br />
<a href='<?php print "{$act_page->guid}?chal={$chal_link}"; ?>'
    target='_blank'>View My Activity</a>

<?php
        if ($use_leader_board) {
            print "&nbsp;&nbsp;<a href='{$lb_page->guid}?chal={$chal_link}' target='_blank'>Leader Board</a>";
        }
    ?>
<br />
<input type='text' id='member-id' placeholder='Member ID...'
	title='Please enter your member ID'
	value='<?php print ($part ? $part->member_id : null); ?>' />
<br />
<input type='text' id='user-name' placeholder='Name...'
	title='Please enter your first and last name'
	value='<?php print ($part ? html_entity_decode($part->name, ENT_QUOTES | ENT_HTML5) : null); ?>' />
<br />
<input type='email' id='user-email' placeholder='Email...'
	title='Please enter your email'
    value='<?php print ($part ? $part->email : null); ?>' />

<?php
        if($chal->use_leader && is_array($chal->leader_list) && count($chal->leader_list)) {
?>
<br />
<select id='leader-id'>
	<option value=''>-- SELECT A GROUP LEADER --</option>
<?php
            foreach($chal->leader_list as $ll) {
                $sel = ($part && $ll->leader_id == $part->leader_id ? " selected" : null);
                print "<option value='{$ll->leader_id}'{$sel}>{$ll->leader_name}</option>";
            }
?>
</select>
<?php
        }
        if ($chal->backdating) { ?>
<br />
<input type='text' id='log-date' placeholder='Activity Date...'
	title='Select the date you want the activity to be for'
	value='<?php print $now->format(get_option('date_format')); ?>' />
<br />
<?php
        }
        if(count($groups)) {
            foreach($chal->activities as $a) {
                $grouped_activities["{$a->group}"][] = $a;
            }
        
            foreach($grouped_activities as $k => $g) {
                print "<h2>{$k}</h2>";
                foreach($g as $act) {
                    Point_Tracker_Pro_Public::print_Activity($act, $part);
                }
                print "<hr />";
            }
        } else {
            foreach($chal->activities as $act) {
                Point_Tracker_Pro_Public::print_Activity($act, $part);
            }
        }
    }
}
