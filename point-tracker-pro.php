<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/godsgood33
 * @since             1.0
 * @package           Point_Tracker_Pro
 *
 * @wordpress-plugin
 * Plugin Name:       Point Tracker Pro
 * Plugin URI:        https://wppointtracker.com
 * Description:       Allow leaders to create challenges and let people track their points.
 * Version:           1.4
 * Author:            Ryan Prather
 * Author URI:        https://wppointtracker.com
 * License:           Apache-2.0
 * License URI:       https://www.apache.org/licenses/LICENSE-2.0
 * Text Domain:       point-tracker-pro
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (! defined('WPINC')) {
    die();
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('PTP_VERSION', '1.4');

/**
 * Constants for plugin updating
 *
 * @var string
 */
define( 'PTP_STORE_URL', 'https://wppointtracker.com' );
define( 'PTP_ITEM_ID', 50 );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-point-tracker-pro-activator.php
 */
function activate_point_tracker_pro()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-point-tracker-pro-activator.php';

    Point_Tracker_Pro_Activator::activate();
    Point_Tracker_Pro_Activator::install_tables();
    Point_Tracker_Pro_Activator::install_functions();
    Point_Tracker_Pro_Activator::create_views();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-point-tracker-pro-deactivator.php
 */
function deactivate_point_tracker_pro()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-point-tracker-pro-deactivator.php';
    Point_Tracker_Pro_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_point_tracker_pro');
register_deactivation_hook(__FILE__, 'deactivate_point_tracker_pro');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-point-tracker-pro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_point_tracker_pro()
{
    $plugin = new Point_Tracker_Pro();
    $plugin->run();
}
run_point_tracker_pro();

if(!class_exists('EDD_SL_Plugin_Updater')) {
    // load our custom updater if it doesn't already exist
    include(dirname(__FILE__) . '/includes/EDD_SL_Plugin_Updater.php');
}

// retrieve our license key from the DB
$license_key = trim(get_option('ptp-license-key', null));
// setup the updater
$edd_updater = new EDD_SL_Plugin_Updater(PTP_STORE_URL, __FILE__, [
    'version' 	=> PTP_VERSION,		// current version number
    'license' 	=> $license_key,	// license key (used get_option above to retrieve from DB)
    'item_id'   => PTP_ITEM_ID,     // id of this plugin
    'author' 	=> 'Ryan Prather',	// author of this plugin
    'url'       => home_url(),
    'beta'      => false // set to true if you wish customers to receive update notifications of beta releases
]);

/**
 * Method to activate a license
 * 
 * @global wpdb $wpdb;
 */
function ptp_activate_license()
{
    if(isset($_POST['ptp-license-activate'])) {
        if(!check_admin_referer('ptp-license-nonce', 'ptp_license_nonce')) {
            return;
        }
            
        $license = trim(get_option('ptp-license-key'));
        
        $api_params = [
            'edd_action' => 'activate_license',
            'license' => $license,
            'item_id' => PTP_ITEM_ID,
            'url' => home_url()
        ];
        
        $res = wp_remote_post(PTP_STORE_URL, [
            'timeout' => 15,
            'sslverify' => false,
            'body' => $api_params
        ]);
        
        if(is_wp_error($res) || wp_remote_retrieve_response_code($res) !== 200) {
            $msg = (is_wp_error($res) && ! empty($res->get_error_message())) ? $res->get_error_message() : __('An error occurred, please try again.');
        } else {
            $ld = json_decode(wp_remote_retrieve_body($res));
            
            if($ld->success === false) {
                switch($ld->error) {
                    case 'expired':
                        $msg = sprintf(
                            __("Your license key expired on %s."),
                            date_i18n(get_option('date_format'), strtotime($ld->expires, current_time('timestamp')))
                        );
                        break;
                    case 'revoked':
                        $msg = __('Your license key has been disabled');
                        break;
                    case 'missing':
                        $msg = __('Invalid license key');
                        break;
                    case 'invalid':
                    case 'site_inactive':
                        $msg = __('Your license is not active for this URL.');
                        break;
                    case 'item_name_mismatch':
                        $msg = sprintf(__('This appears to be an invalid license key for %s.'), 'Point Tracker Pro');
                        break;
                    case 'no_activations_left':
                        $msg = __('Your license key has reached its activation limit.');
                        break;
                    default :
                        $msg = __('An error occurred, please try again.');
                        break;
                }
            }
        }
        
        if(!empty($msg)) {
            $base_url = admin_url('admin.php?page=point-tracker-pro');
            $redirect = add_query_arg(['sl_activation' => 'false', 'message' => urlencode($msg)], $base_url);
            wp_redirect($redirect);
            exit();
        }
        
        if(!remove_all_actions('save_post')) {
            wp_die("Could not remove save_post actions");
        }
        
        $site_url = get_site_url();
        
        $the_page = get_page_by_title("Challenge");
        if (! $the_page->ID) {
            // create page with template
            $post_id = wp_insert_post([
                'post_title' => 'Challenge',
                'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'page',
                'guid' => "{$site_url}/index.php/challenge/",
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => 'challenge'
            ]);
        } else {
            // make sure the page is not trashed...
            $the_page->post_status = 'publish';
            $the_page->post_content = (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : "");
            $the_page->guid = "{$site_url}/index.php/challenge/";
            $post_id = wp_update_post($the_page);
        }
        
        if (! $post_id) {
            wp_die("Failed to save Challenge page");
        }
        
        $the_page = get_page_by_title("Challenge List");
        if (! $the_page->ID) {
            $post_id = wp_insert_post([
                'post_title' => 'Challenge List',
                'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge_list]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'page',
                'guid' => "{$site_url}/index.php/challenge-list/",
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => 'challenge-list'
            ]);
        } else {
            $the_page->post_status = 'publish';
            $the_page->post_content = (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge_list]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : "");
            $the_page->guid = "{$site_url}/index.php/challenge-list/";
            $post_id = wp_update_post($the_page);
        }
        
        if (! $post_id) {
            wp_die("Failed to save Challenge List page");
        }
        
        $the_page = get_page_by_title("My Activity");
        if (! $the_page->ID) {
            $post_id = wp_insert_post([
                'post_title' => 'My Activity',
                'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[my_activity]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'page',
                'guid' => "{$site_url}/index.php/my-activity/",
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => 'my-activity'
            ]);
        } else {
            $the_page->post_status = 'publish';
            $the_page->post_content = (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[my_activity]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : "");
            $the_page->guid = "{$site_url}/index.php/my-activity/";
            $post_id = wp_update_post($the_page);
        }
        
        if (! $post_id) {
            wp_die("Failed to save My Activity page");
        }
        
        $the_page = get_page_by_title("Leader Board");
        if (! $the_page->ID) {
            $post_id = wp_insert_post([
                'post_title' => 'Leader Board',
                'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[leader_board]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'page',
                'guid' => "{$site_url}/index.php/leader-board/",
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => 'leader-board'
            ]);
        } else {
            $the_page->post_status = 'publish';
            $the_page->post_content = (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . '[leader_board]' . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : "");
            $the_page->guid = "{$site_url}/index.php/leader-board/";
            $post_id = wp_update_post($the_page);
        }
        
        if (! $post_id) {
            wp_die("Failed to save Leader Board page");
        }
        
        $the_page = get_page_by_title("Leader List");
        if (! $the_page->ID) {
            $post_id = wp_insert_post([
                'post_title' => 'Leader List',
                'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[leader_list]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => 'page',
                'guid' => "{$site_url}/index.php/leader-list/",
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => 'leader-list'
                    ]);
        } else {
            $the_page->post_status = 'publish';
            $the_page->post_content = (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[leader_list]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : "");
            $the_page->guid = "{$site_url}/index.php/leader-list/";
            $post_id = wp_update_post($the_page);
        }
        
        if (! $post_id) {
            wp_die("Failed to save the Leader List page");
        }
        
        // $license_data->license will be either "valid" or "invalid"
        update_option('ptp-license-status', $ld->license);
        wp_redirect(admin_url('admin.php?page=point-tracker-pro'));
        exit();
    }
}
add_action('admin_init', 'ptp_activate_license');

/**
 * Method to deactivate a license
 */
function ptp_deactivate_license()
{
    if(isset($_POST['ptp-license-deactivate'])) {
        global $wpdb;
        if(!check_ajax_referer('ptp-license-nonce', 'ptp_license_nonce')) {
            return;
        }
        
        $license = trim(get_option('ptp-license-key'));
        
        $api_params = [
            'edd_action' => 'deactivate_license',
            'license' => $license,
            'item_id' => PTP_ITEM_ID,
            'url' => home_url()
        ];
        
        $res = wp_remote_post(PTP_STORE_URL, [
            'timeout' => 15,
            'sslverify' => false,
            'body' => $api_params
        ]);
        
        if(is_wp_error($res) || wp_remote_retrieve_response_code($res) !== 200) {
            $msg = (is_wp_error($res) && ! empty($res->get_error_message())) ? $res->get_error_message() : __('An error occurred, please try again.');
        } else {
            $ld = json_decode(wp_remote_retrieve_body($res));
            
            if($ld->success === false) {
                switch($ld->error) {
                    case 'expired':
                        $msg = sprintf(
                        __("Your license key expired on %s."),
                        date_i18n(get_option('date_format'), strtotime($ld->expires, current_time('timestamp')))
                        );
                        break;
                    case 'revoked':
                        $msg = __('Your license key has been disabled');
                        break;
                    case 'missing':
                        $msg = __('Invalid license key');
                        break;
                    case 'invalid':
                    case 'site_inactive':
                        $msg = __('Your license is not active for this URL.');
                        break;
                    case 'item_name_mismatch':
                        $msg = sprintf(__('This appears to be an invalid license key for %s.'), 'Point Tracker Pro');
                        break;
                    case 'no_activations_left':
                        $msg = __('Your license key has reached its activation limit.');
                        break;
                    default :
                        $msg = __('An error occurred, please try again.');
                        break;
                }
            }
        }
        
        if(!empty($msg)) {
            $base_url = admin_url('admin.php?page=point-tracker-pro');
            $redirect = add_query_arg(['sl_activation' => 'false', 'message' => urlencode($msg)], $base_url);
            wp_redirect($redirect);
            exit();
        }
        
        $chal = get_page_by_title("Challenge");
        wp_update_post([
            'ID' => $chal->ID,
            'post_status' => 'draft'
        ]);
        $chal_list = get_page_by_title("Challenge List");
        wp_update_post([
            'ID' => $chal_list->ID,
            'post_status' => 'draft'
        ]);
        $act = get_page_by_title("My Activity");
        wp_update_post([
            'ID' => $act->ID,
            'post_status' => 'draft'
        ]);
        $lb = get_page_by_title("Leader Board");
        wp_update_post([
            'ID' => $lb->ID,
            'post_status' => 'draft'
        ]);
        $lb_list = get_page_by_title("Leader List");
        wp_update_post([
            'ID' => $lb_list->ID,
            'post_status' => 'draft'
        ]);
        
        $pages = $wpdb->get_results("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[challenge%' AND post_status = 'publish'");
        if(is_array($pages)) {
            foreach($pages as $p) {
                wp_update_post([
                    'ID' => $p->ID,
                    'post_status' => 'draft'
                ]);
            }
        } elseif(isset($pages->ID)) {
            wp_update_post([
                'ID' => $pages->ID,
                'post_status' => 'draft'
            ]);
        }
        
        // $license_data->license will be either "valid" or "invalid"
        update_option('ptp-license-status', $ld->license);
        wp_redirect(admin_url('admin.php?page=point-tracker-pro'));
        exit();
    }
}
add_action('admin_init', 'ptp_deactivate_license');

/**
 * Method to display any admin notices
 */
function adminNotices()
{
    if(isset($_GET['sl_activation']) && !empty($_GET['message'])) {
        if($_GET['sl_activation'] == 'false') {
            $msg = urldecode($_GET['message']);
            print "<div class='notice notice-error'><p>$msg</p></div>";
        } elseif($_GET['sl_activation'] == 'true') {
            print "<div class='notice notice-success is-dismissible'><p>License activation successful. Welcome to Point Tracker Pro!</p></div>";
        }
    }
}
add_action('admin_notices', 'adminNotices');

/**
 * 
 */
function ptp_license_page()
{
    $key = get_option('ptp-license-key', null);
    $status = get_option('ptp-license-status', null);
    ?>
<div class="wrap">
<h2><?php _e("Activate Point Tracker"); ?></h2>
<form method='post' action='options.php'>
	<?php settings_fields("ptp_license"); ?>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row" valign="top"><?php _e("License Key"); ?></th>
				<td>
					<input type='text' name='ptp-license-key' id='license-key' placeholder='License Key...'
						class="regular-text" value="<?php print esc_attr_e($key); ?>" />
        			<label class="description" for='license-key'><?php _e("Enter your license key"); ?></label>
    			</td>
			</tr>
			<?php if($key !== false && strlen($key) > 0) { ?>
			<tr>
				<th scope="row" valign="top"><?php _e("Activate License"); ?></th>
				<td>
					<?php if ($status !== false && $status == 'valid') { ?>
						<span style='color:green;'><?php _e('active'); ?></span>
						<?php wp_nonce_field('ptp-license-nonce', 'ptp_license_nonce'); ?>
						<input type='submit' class='button-secondary' name='ptp-license-deactivate' value='<?php _e('Deactivate License'); ?>' />
					<?php } else { ?>
						<span style='color:red;'><?php _e('inactive'); ?></span>
						<?php wp_nonce_field('ptp-license-nonce', 'ptp_license_nonce'); ?>
						<input type='submit' class='button-secondary' name='ptp-license-activate' value='<?php _e('Activate License'); ?>' />
					<?php } ?>
				</td>
			</tr>
			<?php } ?>
		</tbody>
    </table>
	<?php if($status != 'valid') submit_button(); ?>
</form>
<?php
}

function ptp_license_menu() {
    if(get_option('ptp-license-status') != 'valid') {
        add_menu_page('Point Tracker', 'Point Tracker', 'manage_options', 'point-tracker-pro', 'ptp_license_page', 'dashicons-admin-generic', 2);
    }
}
add_action('admin_menu', 'ptp_license_menu');

function ptp_register_option() {
    // creates our settings in the options table
    register_setting('ptp_license', 'ptp-license-key', 'ptp_sanitize_license');
}
add_action('admin_init', 'ptp_register_option');

function ptp_sanitize_license($new) {
    $old = get_option('ptp-license-key');
    if($old && $old != $new) {
        update_option('ptp-license-status', ''); // new license has been entered, so must reactivate
    }
    return $new;
}

/**
 * Static method to check purchase
 *
 * @param string $strKey
 *
 * @return boolean
 *      Returns TRUE if a valid license, FALSE otherwise
 */
function verifyPurchase($strKey)
{
    $store_url = 'https://wppointtracker.com';
    $item_name = 'Point Tracker Pro';
    $api_params = [
        'edd_action' => 'check_license',
        'license' => $strKey,
        'item_name' => urlencode($item_name),
        'url' => home_url()
    ];
    $response = wp_remote_post($store_url, ['body' => $api_params, 'timeout' => 15, 'sslverify' => false]);
    if (is_wp_error($response)) {
        return false;
    }
    
    $license_data = json_decode(wp_remote_retrieve_body($response));
    
    update_option('ptp-license-status', ($license_data->license == 'valid' ? 'valid' : 'invalid'));
    
    return ($license_data->license === 'valid');
}
