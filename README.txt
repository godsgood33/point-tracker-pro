=== Plugin Name ===
Point Tracker Pro
Contributors: godsgood33
Tags: team activities, challenge, content
Requires at least: 4.4.2
Requires PHP: 5.6
Tested up to: 5.0.2
Stable tag: 1.4
License: Apache-2.0
License URI: https://www.apache.org/licenses/LICENSE-2.0

This plugin will allow site admins to create challenges and then participants can enter their activity.

== Description ==

This plugin does not require but works well with membership plugins.  The admin can create a challenge, share the challenge link with whom they wish.  Those wishing to participate can click on the link and opt to join the challenge. Once the participant has joined the challenge, they can enter activity against that challenge upto daily and receive points.

== Installation ==

This section describes how to install the plugin and get it working.

1. Extract the 'point-tracker-pro.zip' file in the '/wp-content/plugins/' directory or install using the plugin installer in WP
2. Activate the plugin through the 'Plugins' menu in WordPress
3. WP will then direct you to enter an activiation code.  Copy/paste the code from the confirmation email you received, click "Save", then after the page refreshes you'll see a "Activate License" button, click that to start using Point Tracker Pro
4. The Point Tracker admin menu is used to administer the challenges, their activities and participants. 
5. Navigate to the `Point Tracker` -> `Challenges` admin menu
6. Fill out the form and create a challenge (add a name, start and end dates, and a description) then click "Save"
7. Copy the link that appears just above the description box
8. Navigate to the `Point Tracker` -> `Activities` admin menu
9. Select the challenge you just created from the drop down
10. Fill out the boxes and create your first activity for that challenge and click "Save" then repeat for all activities you want participants to complete
11. Send the link to whomever you wish so they can join
12. Under `Settings` -> `Point Tracker` there are global options that you can enable

== Frequently Asked Questions ==

= What happens if I initially set a challenge for approval required, then change it? =

The challenge will automatically approve all pending participants and any future participants will automatically be approved

= Once the challenge is over what happens? =

You will need to visit the Point Tracker -> Participants page to see who has the most points

= A participant entered some wrong information, what do I do? =

You can either delete it yourself or if they visit the View My Activity page (/my-activity/?chal={linkcode}), they will be able to delete it themselves

= How can I create a custom page? =

After you've created a challenge, click the "Create Page" button just above the description.  You can also copy/paste the unique code for the challenge into a new shortcode `[challenge chal={challenge code you copied}][/challenge]`, then publish the page.  You can also create custom pages for the My Activity and Leader Board pages using their respective shortcodes (`[my_activity chal={code}][/my_activity]` and `[leader_board chal={code]][/leader_board]`).

= I'd like a leader board =

A leader board is available in the admin menu.  Just navigate to `Point Tracker` -> `Leader Board`, and select the challenge you want to see.  If you would like to make a Leader Board available to the participants of the challenge you can select the Leader Board checkbox when creating the challenge and select the number of participants you want to display on that leader board.  By default, it will display all members but you can select 1, 3, 5, 10, 25, 50, or 100 participants.

= What is a private challenge? =

A private challenge is one that people cannot join themselves.  It is up to the challenge coordinator to add them manually or via the participant upload option.  It is similar to the "Approval Required" option.

= How do randomly select a winner? =

Follow the instructions on the `Settings` -> `Point Tracker` page to get a Random.org API code
Go to the `Point Tracker` -> `Random Drawing` page
Select the challenge you want to draw the random winner from.  You can filter this list by additionally selecting an activity, leader (if groups are being used in the challenge), and minimum points required.
You can also give all users one entry for each point they received for list by clicking the checkbox
Then click the "Select Winner".  The program will pseudo-randomize the list, contact Random.org to get a random number between 1 and 'n' (number of total entries possible between all participants), then using that number will find the participant that corresponds to that random number and alert the admin  

== Changelog ==

= 1.4 =
* Add winner mark enhancement
* Fixed bug with uploading CSV participant list

= 1.3.1 =
* Bug fix from updating JSON-RPC library

= 1.3 =
* Added private challenges
* Gutenberg/WP5+ compatibility
* Bug fixes

= 1.2 =
* Added new Point Tracker -> Random Selection page that will let you randomly select a winner from your challenge and filter it by activity if you wish.
* Fixed a couple bugs

= 1.1 =
* Fixed a couple bug with saving entries and long text entries

= 1.0 =
* Initial release
* All the same functionality as the free version of Point Tracker and THEN SOME!!

== Upgrade Notice ==

Upgrade section