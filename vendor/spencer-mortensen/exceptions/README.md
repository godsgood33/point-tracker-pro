# Exceptions

This project is available as a Composer Package:   
[spencer-mortensen/exceptions](https://packagist.org/packages/spencer-mortensen/exceptions)


## Unit tests

This project uses the [Lens](http://lens.guide) unit-testing framework.

[![Build Status](https://travis-ci.org/spencer-mortensen/exceptions.png?branch=master)](https://travis-ci.org/spencer-mortensen/exceptions)
