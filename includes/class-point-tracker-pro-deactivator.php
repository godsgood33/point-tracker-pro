<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/godsgood33
 * @since      1.0.0
 *
 * @package    Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since 1.0.0
 * @package Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/includes
 * @author Ryan Prather <godsgood33@gmail.com>
 */
class Point_Tracker_Pro_Deactivator
{

    /**
     * Function called when deactivating the plugin
     *
     * @since 1.0.0
     */
    public static function deactivate()
    {
        if(!remove_all_actions('save_post')) {
            wp_die("Could not remove save_post actions");
        }

        delete_option('ptp-require-login');
        delete_option('ptp-email-new-participants');
        delete_option('ptp-admin-summary-email');

        $the_page = get_page_by_title("Challenge");
        wp_update_post([
            'ID' => $the_page->ID,
            'post_status' => 'draft'
        ]);

        $the_page = get_page_by_title("Challenge List");
        wp_update_post([
            'ID' => $the_page->ID,
            'post_status' => 'draft'
        ]);

        $the_page = get_page_by_title("My Activity");
        wp_update_post([
            'ID' => $the_page->ID,
            'post_status' => 'draft'
        ]);

        $the_page = get_page_by_title("Leader Board");
        wp_update_post([
            'ID' => $the_page->ID,
            'post_status' => 'draft'
        ]);

        $the_page = get_page_by_title("Leader List");
        wp_update_post([
            'ID' => $the_page->ID,
            'post_status' => 'draft'
        ]);
    }
}
