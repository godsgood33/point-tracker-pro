<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bitbucket.org/godsgood33
 * @since      1.0.0
 *
 * @package    Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since 1.0.0
 * @package Point_Tracker_Pro
 * @subpackage Point_Tracker_Pro/includes
 * @author Ryan Prather <godsgood33@gmail.com>
 */
class Point_Tracker_Pro_i18n
{

    /**
     * Load the plugin text domain for translation.
     *
     * @since 1.0.0
     */
    public function load_plugin_textdomain()
    {
        load_plugin_textdomain(
			'point-tracker-pro',
			false,
			dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
		);
    }
}
