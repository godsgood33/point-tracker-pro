<?php
add_action('wp_ajax_get-participants', 'ptp_get_participant_table');
add_action('wp_ajax_approve-participant', 'ptp_approve_participant');
add_action('wp_ajax_remove-participant', 'ptp_remove_participant');
add_action('wp_ajax_add-participant', 'ptp_add_participant');
add_action('wp_ajax_join-challenge', 'ptp_join_challenge');
add_action('wp_ajax_clear-participants', 'ptp_clear_participants');
add_action('wp_ajax_mark-winner', 'pt_mark_winner');

/**
 * Function to write a CSV file with the participant list for a leader
 *
 * @global wpdb $wpdb
 *
 * @param string $fname
 * @param int $leader_id
 *
 * @return string
 */
function ptp_write_participant_list($fname, $leader_id)
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $query = $wpdb->prepare("CREATE TEMPORARY TABLE tmp_log
SELECT IF(ca.type = 'number',(ca.points * al.value),ca.points) AS 'total_points',ll.leader_name,cp.*
FROM {$wpdb->prefix}pt_participants cp
LEFT JOIN {$wpdb->prefix}pt_log al ON al.user_id = cp.user_id
LEFT JOIN {$wpdb->prefix}pt_activities ca on ca.id = al.activity_id AND ca.challenge_id = cp.challenge_id
LEFT JOIN {$wpdb->prefix}pt_leader_list ll ON ll.leader_id = cp.leader_id
WHERE cp.challenge_id = %d AND
    ll.leader_id = %d
GROUP BY cp.user_id, al.activity_id, al.log_date", $chal_id, $leader_id);
    $wpdb->query($query);

    $query = "SELECT SUM(al.total_points) AS 'total',al.*
FROM tmp_log al
GROUP BY al.user_id";
    $participants = $wpdb->get_results($query);

    $fh = fopen($fname, "w");
    fputcsv($fh, [
        'Member ID',
        'Name',
        'Email',
        'Total'
    ]);
    foreach ($participants as $part) {
        fputcsv($fh, [
            $part->member_id,
            $part->name,
            $part->email,
            $part->total
        ]);
    }
    fclose($fh);

    return true;
}

/**
 * Function to get all the challenge participants
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded array of stdClass objects of all challenge participants
 */
function ptp_get_participant_table()
{
    global $wpdb;
    $_data = [];

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));

    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("CREATE TEMPORARY TABLE tmp_log
SELECT IF(ca.type = 'number',(ca.points * al.value),ca.points) AS 'total_points',ll.leader_name,cp.*
FROM {$wpdb->prefix}pt_participants cp
LEFT JOIN {$wpdb->prefix}pt_log al ON al.user_id = cp.user_id
LEFT JOIN {$wpdb->prefix}pt_activities ca on ca.id = al.activity_id AND ca.challenge_id = cp.challenge_id
LEFT JOIN {$wpdb->prefix}pt_leader_list ll ON ll.leader_id = cp.leader_id
WHERE cp.challenge_id = %d
GROUP BY cp.user_id, al.activity_id, al.log_date", $chal_id);
    $wpdb->query($query);

    $query = "SELECT SUM(al.total_points) AS 'total',al.*
FROM tmp_log al
GROUP BY al.user_id";
    $participants = $wpdb->get_results($query);

    $winner_idx = null;
    foreach ($participants as $x => $part) {
        $_data[] = [
            'approved' => "<input type='checkbox' " . (! $chal->approval ? "disabled" : "") . " class='approve' " . ((boolean) $part->approved ? " checked" : "") . " data-user-id='{$part->user_id}' />",
            'memberid' => $part->member_id,
            'name' => html_entity_decode($part->name, ENT_QUOTES | ENT_HTML5),
            'email' => sanitize_email($part->email),
            'totalPoints' => $part->total,
            'leader_name' => html_entity_decode($part->leader_name, ENT_QUOTES | ENT_HTML5),
            'action' => "<i class='far fa-trash-alt' title='Remove this participant from the activity' data-user-id='{$part->user_id}'></i>" .
                "&nbsp;&nbsp;" .
                "<i class='fas fa-trophy' title='Mark participant as winner' data-user-id='{$part->user_id}'></i>",
            'winner' => ($chal->winner == $part->user_id ? true : false)
        ];
        
        if($chal->winner == $part->user_id) {
            $winner_idx = $x;
        }
    }
    
    $columns = [
        [
            'title' => 'Approved',
            'defaultContent' => "",
            'data' => 'approved'
        ],
        [
            'title' => 'Member ID',
            'defaultContent' => '',
            'data' => 'memberid'
        ],
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'name'
        ],
        [
            'title' => 'Email',
            'defaultContent' => '',
            'data' => 'email'
        ],
        [
            'title' => 'Total Points',
            'defaultContent' => 0,
            'data' => 'totalPoints'
        ],
        [
            'title' => 'Action',
            'defaultContent' => '',
            'data' => 'action'
        ]
    ];

    $ll = null;
    if ($chal->use_leader) {
        $columns = ptp_insert_array($columns, 5, [
            'title' => 'Leader',
            'defaultContent' => '',
            'data' => 'leader_name'
        ]);

        $query = $wpdb->prepare("SELECT leader_id,leader_name,leader_email,leader_member_id
FROM {$wpdb->prefix}pt_leader_list
WHERE challenge_id = %d
ORDER BY leader_name", $chal_id);
        $ll = $wpdb->get_results($query);

        foreach ($ll as $k => $l) {
            $ll[$k]->leader_name = html_entity_decode($l->leader_name, ENT_QUOTES | ENT_HTML5);
        }

        if (! $ll) {
            print json_encode([
                'error' => 'There are no leaders assigned to this challenge'
            ]);
            wp_die();
        }
    }

    print json_encode([
        'columns' => $columns,
        'data' => $_data,
        'll' => $ll,
        'use_leader' => (boolean) $chal->use_leader,
        'winner' => $winner_idx
    ]);

    wp_die();
}

/**
 * Function to approve a participant
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the requested approval operation
 */
function ptp_approve_participant()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You are not the coordinator for this challenge (access denied)'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $user_id = filter_input(INPUT_POST, 'user-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $res = $wpdb->update("{$wpdb->prefix}pt_participants", [
        'approved' => 1
    ], [
        'challenge_id' => $chal_id,
        'user_id' => $user_id
    ]);

    if ($res) {
        $email = $wpdb->get_var($wpdb->prepare("SELECT email FROM {$wpdb->prefix}pt_participants WHERE user_id = %d AND challenge_id = %d", $user_id, $chal_id));
        if (get_option('ptp-email-new-participants', 0) && $email) {
            wp_mail($email, 'Approved for Team Challenge', PTP_USER_APPROVED);
        }

        print json_encode([
            'success' => 'Participant has been approved for the challenge'
        ]);
    } else {
        print json_encode([
            'error' => $wpdb->last_error
        ]);
    }

    wp_die();
}

/**
 * Function to remove a participant from a challenge
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the requested removal operation
 */
function ptp_remove_participant()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You are not the coordinator of this challenge (access denied)'
        ]);
        wp_die();
    } elseif (! check_ajax_referer('ptp-delete-participant', 'security', false)) {
        print json_encode([
            'error' => 'We were unable to verify the nonce'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $user_id = filter_input(INPUT_POST, 'user-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $email = $wpdb->get_var($wpdb->prepare("SELECT email FROM {$wpdb->prefix}pt_participants WHERE user_id = %d AND challenge_id = %d", $user_id, $chal_id));

    $query = $wpdb->prepare("DELETE al.*
FROM {$wpdb->prefix}pt_log al
JOIN {$wpdb->prefix}pt_participants cp ON cp.user_id = al.user_id
WHERE
    cp.challenge_id = %d AND
    cp.user_id = %d", $chal_id, $user_id);
    $wpdb->query($query);

    $res = $wpdb->delete("{$wpdb->prefix}pt_participants", [
        'challenge_id' => $chal_id,
        'user_id' => $user_id
    ]);
    
    $winner_id = $wpdb->get_var($wpdb->prepare("SELECT winner FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if($winner_id == $user_id) {
        $wpdb->update("{$wpdb->prefix}pt_challenges", ['winner' => null], ['id' => $chal_id]);
    }

    if ($res) {
        if (get_option('ptp-email-new-participants', 0) && $email) {
            wp_mail($email, 'Removed from Team Challenge', PTP_USER_DENIED);
        }

        print json_encode([
            'success' => 'User was removed from the challenge'
        ]);
    } else {
        print json_encode([
            'error' => $wpdb->last_error
        ]);
    }

    wp_die();
}

/**
 * Function to manually add a participant
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the requested operation
 */
function ptp_add_participant()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You are not the coordinator of this challenge (access denied)'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    $member_id = filter_input(INPUT_POST, 'member-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $name = sanitize_text_field(filter_input(INPUT_POST, 'user-name', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    $email = strtolower(sanitize_email(filter_input(INPUT_POST, 'user-email', FILTER_SANITIZE_EMAIL, FILTER_NULL_ON_FAILURE)));
    $leader_id = filter_input(INPUT_POST, 'leader-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $now = new DateTime("now", new DateTimeZone(get_option("timezone_string")));

    if (! $member_id) {
        print json_encode([
            'error' => 'Member ID must be numeric'
        ]);
        wp_die();
    } elseif (! $name) {
        print json_encode([
            'error' => "Must specify the user's name"
        ]);
        wp_die();
    } elseif (! $email) {
        print json_encode([
            'error' => "Must specify the user's e-mail"
        ]);
        wp_die();
    } elseif ($chal->use_leader && ! $leader_id) {
        print json_encode([
            'error' => 'Invalid leader selection'
        ]);
        wp_die();
    } elseif ($chal->use_leader) {
        $query = $wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d AND leader_id = %d", $chal->id, $leader_id);
        $valid_leader = (boolean) $wpdb->get_var($query);

        if (! $valid_leader) {
            print json_encode([
                'error' => 'Invalid leader selection (not participant leader)'
            ]);
            wp_die();
        }
    }

    if ($uid = email_exists($email)) {
        $res = $wpdb->insert("{$wpdb->prefix}pt_participants", [
            'challenge_id' => $chal_id,
            'user_id' => $uid,
            'name' => $name,
            'email' => $email,
            'member_id' => $member_id,
            'leader_id' => $leader_id,
            'approved' => 1,
            'date_joined' => $now->format("Y-m-d"),
            'date_approved' => $now->format("Y-m-d")
        ]);
    } else {
        // generate a random password and create an account
        $random_pwd = wp_generate_password();
        $uid = wp_create_user($email, $random_pwd, $email);

        $res = $wpdb->insert("{$wpdb->prefix}pt_participants", [
            'challenge_id' => $chal_id,
            'user_id' => $uid,
            'name' => $name,
            'email' => $email,
            'member_id' => $member_id,
            'leader_id' => $leader_id,
            'approved' => 1,
            'date_joined' => $now->format("Y-m-d"),
            'date_approved' => $now->format("Y-m-d")
        ]);
    }

    print json_encode($res ? [
        'success' => 'Successfully added participant',
        'user_id' => $uid,
        'name' => $name,
        'email' => $email
    ] : [
        'error' => $wpdb->last_error
    ]);

    if (get_option("ptp-email-new-participants", 0) && $res !== false) {
        wp_mail("{$name} <{$email}>", "Added to challenge", str_replace([
            "{name}",
            "{desc}"
        ], [
            html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5),
            html_entity_decode($chal->desc, ENT_QUOTES | ENT_HTML5)
        ], PTP_USER_ADDED));
    }

    wp_die();
}

/**
 * Function to allow a participant to join a challenge
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the requested join operation
 */
function ptp_join_challenge()
{
    global $wpdb;

    $chal_link = filter_input(INPUT_POST, 'chal-link', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $member_id = filter_input(INPUT_POST, 'member-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $leader_id = filter_input(INPUT_POST, 'leader', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE short_link = %s", $chal_link);
    $chal = $wpdb->get_row($query);

    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    if ($leader_id) {
        $query = $wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_leader_list WHERE leader_id = %d AND challenge_id = %d", $leader_id, $chal->id);
        $valid_leader = (boolean) $wpdb->get_var($query);

        if (! $valid_leader) {
            print json_encode([
                'error' => 'That leader is not part of this challenge'
            ]);
            wp_die();
        }
    }

    $name = sanitize_text_field(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    $email = strtolower(sanitize_email(filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE)));

    $now = new DateTime("now", new DateTimeZone(get_option("timezone_string")));

    $res = $wpdb->insert("{$wpdb->prefix}pt_participants", [
        'challenge_id' => $chal->id,
        'user_id' => get_current_user_id(),
        'approved' => ($chal->approval ? '0' : '1'),
        'date_joined' => $now->format("Y-m-d"),
        'date_approved' => ($chal->approval ? null : $now->format("Y-m-d")),
        'member_id' => $member_id,
        'name' => $name,
        'email' => $email,
        'leader_id' => $leader_id
    ]);

    $chal_name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);

    if (get_option('admin_email', null)) {
        wp_mail(get_option('admin_email'), "Participant joined {$chal_name}", str_replace([
            "{name}",
            "{chal}"
        ], [
            html_entity_decode($name, ENT_QUOTES | ENT_HTML5),
            $chal_name
        ], PTP_NEW_PARTICIPANT));
    }

    print json_encode($res ? [
        'success' => ($chal->approval ? "Requested to join" : "Joined challenge"),
        'redirect' => !$chal->approval
    ] : [
        'error' => 'Unknown error'
    ]);

    wp_die();
}

/**
 * Function to remove all participants and log from a challenge
 *
 * @global wpdb $wpdb
 */
function ptp_clear_participants()
{
    global $wpdb;
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $query = $wpdb->prepare("DELETE l.* FROM {$wpdb->prefix}pt_log l JOIN {$wpdb->prefix}pt_participants p ON p.user_id = l.user_id WHERE p.challenge_id = %d", $chal_id);
    $res = $wpdb->query($query);

    if ($res) {
        $query = $wpdb->prepare("DELETE FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d", $chal_id);
        $res = $wpdb->query($query);
    }

    print json_encode($res ? [
        'success' => "Deleted participants from challenge"
    ] : [
        'error' => $wpdb->last_error
    ]);
    wp_die();
}

/**
 * Function to insert an element into a specific position in the array
 *
 * @param array $array
 * @param int $index
 * @param mixed $val
 *
 * @return mixed
 */
function ptp_insert_array($array, $index, $val)
{
    $size = count($array); // because I am going to use this more than one time
    if (! is_int($index) || $index < 0 || $index > $size) {
        return - 1;
    } else {
        $temp = array_slice($array, 0, $index);
        $temp[] = $val;
        return array_merge($temp, array_slice($array, $index, $size));
    }
}

/**
 * Function to upload a CSV file and import all the users to the database and get them registered for the challenge
 *
 * @global wpdb $wpdb
 *
 * @param string $fname
 */
function ptp_participant_upload($fname)
{
    global $wpdb;
    set_time_limit(0);

    // verify a valid challenge id was entered and the challenge can be found from that info
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    if (! $chal_id) {
        wp_die("Invalid challenge ID");
    }
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        wp_die("Unable to find the selected challenge");
    }

    // get the local upload path
    $absname = "{$fname}";

    if (! file_exists($absname) || ! is_readable($absname)) {
        unlink($absname);
        wp_die("Unable to access the uploaded file");
    }

    // open the file, get the first line and make sure it is a valid header
    $fh = fopen($absname, "r");
    $h = explode(',', trim(strtolower(fgets($fh))));
    $h = array_flip($h);

    if (! isset($h['id']) || ! isset($h['name']) || ! isset($h['email'])) {
        unlink($absname);
        wp_die("Missing ID, Name, or Email field");
    }

    $leaders = [];
    if (isset($h['leader id']) && $chal->use_leader) {
        $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d", $chal_id);
        $rows = $wpdb->get_results($query);

        foreach ($rows as $row) {
            $leaders["{$row->leader_member_id}"] = $row->leader_id;
        }
    }

    $dt = new DateTime("now", new DateTimeZone(get_option('timezone_string')));

    // loop through the file
    while ($row = fgetcsv($fh)) {
        // sanitize and validate the info in the file
        $id = filter_var($row[$h['id']], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
        $name = sanitize_text_field(filter_var($row[$h['name']], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
        $email = strtolower(sanitize_email($row[$h['email']]));

        // see if the user already has an account on the website
        $uid = email_exists($email);
        if (! $uid) {
            $random_pwd = wp_generate_password();
            $uid = wp_create_user($email, $random_pwd, $email);
        }

        if (! $uid) {
            continue;
        }

        // see if the user is already enrolled in the challenge
        $part = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d AND user_id = %d", $chal_id, $uid));

        if ($part) {
            continue;
        }

        // see if a leader was assigned to the user
        $luid = null;
        if (isset($h['leader id']) && $row[$h['leader id']] && $chal->use_leader) {
            $leader_id = filter_var($row[$h['leader id']], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
            $luid = (isset($leaders["{$leader_id}"]) ? $leaders["{$leader_id}"] : null);
        }

        $res = $wpdb->insert("{$wpdb->prefix}pt_participants", [
            'challenge_id' => $chal_id,
            'user_id' => $uid,
            'leader_id' => ($luid ? $luid : null),
            'email' => $email,
            'member_id' => $id,
            'name' => $name,
            'approved' => 1,
            'date_joined' => $dt->format("Y-m-d"),
            'date_approved' => $dt->format("Y-m-d")
        ]);

        if ($res === false) {
            wp_die("Failed to add a user to the challenge");
        }

        if (get_option('ptp-email-new-participant', 0) && $email && ($chal->approval || get_option('ptp-require-login', 0) || $chal->require_login)) {
            wp_mail($email, 'Approved for Team Challenge', str_replace(["{username}", "{password}"], [$email, $random_pwd], PTP_NEW_USER));
        } elseif (get_option('ptp-email-new-participant', 0) && $email) {
            wp_mail($email, 'Approved for Team Challenge', PTP_USER_APPROVED);
        }
    }

    $wpdb->delete($wpdb->posts, [
        'guid' => "{$fname}"
    ]);

    fclose($fh);
    unlink($absname);
}

/**
 * Function to mark a participant as the winner of a challenge
 *
 * @global wpdb $wpdb
 */
function pt_mark_winner()
{
    global $wpdb;
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $user_id = filter_input(INPUT_POST, 'user-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    
    if(!$chal) {
        print json_encode(['error' => 'Unable to find the challenge selected']);
        wp_die();
    }
    
    $user = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_participants WHERE user_id = %d AND challenge_id = %d", $user_id, $chal_id));
    
    if(!$user) {
        print json_encode(['error' => 'Unable to find that challenge participant']);
        wp_die();
    }
    
    if(!$wpdb->update("{$wpdb->prefix}pt_challenges", ['winner' => $user_id], ['id' => $chal_id])) {
        print json_encode(['error' => $wpdb->last_error]);
        wp_die();
    }
    
    print json_encode(['success' => "Congratulations, {$user->name}, has been marked the winner of the {$chal->name}"]);
    wp_die();
}
