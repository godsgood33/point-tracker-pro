<?php
// logged in user ajax requests
add_action('wp_ajax_get-log', 'ptp_get_log_table');
add_action('wp_ajax_get-challenge-leader-board', 'ptp_get_leader_board');
add_action('wp_ajax_delete-participant-activity', 'ptp_delete_participant_activity');
add_action('wp_ajax_save-entry', 'ptp_participant_save_entry');
add_action('wp_ajax_get-random-selector-data', 'ptp_get_random_selector_data');
add_action('wp_ajax_select-random-winner', 'ptp_select_random_winner');

// public ajax requests
add_action('wp_ajax_nopriv_save-entry', 'ptp_participant_save_entry');
add_action('wp_ajax_nopriv_delete-participant-activity', 'ptp_delete_participant_activity');
add_action('wp_ajax_nopriv_get-my-activity', 'ptp_get_my_activity_table');
add_action('wp_ajax_nopriv_get-leader-log', 'ptp_get_leader_log_table');

require_once WP_PLUGIN_DIR . "/point-tracker-pro/vendor/autoload.php";

use Datto\JsonRpc\Http\Client;

/**
 * Function to write the participant log to a CSV file for emailing to the leader
 *
 * @param string $fname
 * @param int $leader_id
 */
function ptp_write_participant_log($fname, $leader_id)
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $query = $wpdb->prepare("CREATE TEMPORARY TABLE tmp_log SELECT
al.activity_id,al.log_date,al.log_time,al.value,ca.question,ca.points,cp.*
FROM {$wpdb->prefix}pt_log al
JOIN {$wpdb->prefix}pt_activities ca ON ca.id = al.activity_id
JOIN {$wpdb->prefix}pt_participants cp ON cp.user_id = al.user_id AND cp.challenge_id = ca.challenge_id
WHERE ca.challenge_id = %d", $chal_id);
    $wpdb->query($query);

    $query = "SELECT al.*
FROM tmp_log al
ORDER BY al.log_date,al.log_time";
    $log_res = $wpdb->get_results($query);

    $fh = fopen($fname, "w");
    fputcsv($fh, [
        'Name',
        'Question',
        'Points',
        'Date',
        'Answer'
    ]);
    if (is_array($log_res) && count($log_res)) {
        foreach ($log_res as $log) {
            $dt = new DateTime($log->log_date . " " . $log->log_time);
            fputcsv($fh, [
                $log->name,
                $log->question,
                $log->points,
                $dt->format(get_option("date_format", "n/j/Y")),
                $log->value
            ]);
        }
    }
    fclose($fh);
}

/**
 * Function to get the activity log for all participants
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded array of all participant activity
 */
function ptp_get_log_table()
{
    global $wpdb;
    $columns = [
        [
            'title' => 'ID',
            'defaultContent' => '',
            'data' => 'id'
        ],
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'name'
        ],
        [
            'title' => 'Activity',
            'defaultContent' => '',
            'data' => 'activity'
        ],
        [
            'title' => 'Points',
            'defaultContent' => '',
            'data' => 'points'
        ],
        [
            'title' => 'Date',
            'defaultContent' => '',
            'data' => 'dt'
        ],
        [
            'title' => 'Answer',
            'defaultContent' => '',
            'data' => 'answer'
        ],
        [
            'title' => 'Action',
            'defaultContent' => '',
            'data' => 'action'
        ]
    ];

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("CREATE TEMPORARY TABLE tmp_log SELECT
al.activity_id,al.log_date,al.log_time,al.value,ca.question,ca.points,cp.*
FROM {$wpdb->prefix}pt_log al
JOIN {$wpdb->prefix}pt_activities ca ON ca.id = al.activity_id
JOIN {$wpdb->prefix}pt_participants cp ON cp.user_id = al.user_id AND cp.challenge_id = ca.challenge_id
WHERE ca.challenge_id = %d", $chal_id);
    $wpdb->query($query);
    
    $leader_sql = null;
    $leader_field = null;
    if($chal->use_leader) {
        $leader_field = ", ll.leader_name";
        $leader_sql = "LEFT JOIN {$wpdb->prefix}pt_leader_list ll ON ll.leader_id = al.leader_id AND ll.challenge_id = al.challenge_id";
        $columns = [
            [
                'title' => 'ID',
                'defaultContent' => '',
                'data' => 'id'
            ],
            [
                'title' => 'Name',
                'defaultContent' => '',
                'data' => 'name'
            ],
            [
                'title' => 'Leader',
                'defaultContent' => '',
                'data' => 'leader'
            ],
            [
                'title' => 'Activity',
                'defaultContent' => '',
                'data' => 'activity'
            ],
            [
                'title' => 'Points',
                'defaultContent' => '',
                'data' => 'points'
            ],
            [
                'title' => 'Date',
                'defaultContent' => '',
                'data' => 'dt'
            ],
            [
                'title' => 'Answer',
                'defaultContent' => '',
                'data' => 'answer'
            ],
            [
                'title' => 'Action',
                'defaultContent' => '',
                'data' => 'action'
            ]
        ];
    }

    $query = "SELECT al.member_id, al.name, al.question, al.points, al.log_date, al.log_time, al.value, al.activity_id, al.user_id{$leader_field}
FROM tmp_log al
$leader_sql
ORDER BY al.log_date,al.log_time";
    $log_res = $wpdb->get_results($query);

    $_data = [];

    if (is_array($log_res) && count($log_res)) {
        foreach ($log_res as $log) {
            $dt = new DateTime($log->log_date . " " . $log->log_time, new DateTimeZone(get_option('timezone_string')));
            $_data[] = [
                'id' => $log->member_id,
                'name' => html_entity_decode($log->name, ENT_QUOTES | ENT_HTML5),
                'activity' => html_entity_decode($log->question, ENT_QUOTES | ENT_HTML5),
                'points' => $log->points,
                'dt' => $dt->format(get_option('date_format', 'Y-m-d')),
                'answer' => html_entity_decode($log->value, ENT_QUOTES | ENT_HTML5),
                'action' => "<i class='far fa-trash-alt' title='Delete this activity so you can reinput with the correct info' data-act-id='{$log->activity_id}' data-log-date='{$dt->format("Y-m-d")}' data-user-id='{$log->user_id}'></i>",
                'leader' => $log->leader_name
            ];
        }
    }
    
    print json_encode([
        'data' => $_data,
        'columns' => $columns
    ]);
    wp_die();
}

/**
 * Function to save a participants response to an activity
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the requested save operation
 */
function ptp_participant_save_entry()
{
    global $wpdb;
    $wpdb->suppress_errors = true;
    $wpdb->show_errors = false;
    $altered = false;
    $now = new DateTime("now", new DateTimeZone(get_option('timezone_string')));
    $date_format = get_option('date-format', 'Y-m-d');

    $act_id = filter_input(INPUT_POST, 'act-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $log_date = filter_input(INPUT_POST, 'log-date', FILTER_VALIDATE_REGEXP, [
        'options' => [
            'regexp' => "/[\d\\]{10,12}/"
        ]
    ]);
    $member_id = filter_input(INPUT_POST, 'member-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $user_name = sanitize_text_field(filter_input(INPUT_POST, 'user-name', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    $user_email = strtolower(sanitize_email(filter_input(INPUT_POST, 'user-email', FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE)));
    $leader_id = sanitize_text_field(filter_input(INPUT_POST, 'leader-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE));

    if (! $member_id) {
        print json_encode([
            'warning' => 'That is an invalid value for your member ID'
        ]);
        wp_die();
    } elseif (! $user_name) {
        print json_encode([
            'warning' => 'You must put your name on the form'
        ]);
        wp_die();
    } elseif (! $user_email) {
        print json_encode([
            'warning' => 'You must put your e-mail on the form'
        ]);
        wp_die();
    }

    if (isset($log_date)) {
        $now = new DateTime($log_date, new DateTimeZone(get_option('timezone_string')));
    }

    if (! is_a($now, 'DateTime')) {
        print json_encode([
            'error' => 'Were not able to parse the activity date'
        ]);
        wp_die();
    }

    $req_login = (boolean) get_option('ptp-require-login', 0);
    $query = $wpdb->prepare("SELECT c.*
FROM {$wpdb->prefix}pt_challenges c
JOIN {$wpdb->prefix}pt_activities ca ON ca.challenge_id = c.id
WHERE ca.id = %d", $act_id);
    $chal = $wpdb->get_row($query);

    if(!$chal) {
        print json_encode([
            'error' => 'Unable to find that selected challenge'
        ]);
        wp_die();
    } elseif ($chal->use_leader && ! $leader_id) {
        print json_encode([
            'warning' => 'You must select a leader you are assigned to'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_activities WHERE id = %d", $act_id);
    $act = $wpdb->get_row($query);

    $act_start = null;
    if($act->start_dt) {
        $act_start = new DateTime($act->start_dt, new DateTimeZone(get_option('timezone_string')));
    }
    $act_end = null;
    if($act->end_dt) {
        $act_end = new DateTime("{$act->end_dt} 23:59:59", new DateTimeZone(get_option('timezone_string')));
    }
    $start = new DateTime($chal->start, new DateTimeZone(get_option('timezone_string')));
    $end = new DateTime("{$chal->end} 23:59:59", new DateTimeZone(get_option('timezone_string')));

    // Verify the challenge is still running
    if ($now < $start) {
        print json_encode([
            'error' => "Challenge hasn't started"
        ]);
        wp_die();
    } elseif ($now > $end) {
        print json_encode([
            'error' => "Challenge is already over"
        ]);
        wp_die();
    } elseif (!is_null($act_start) && $now < $act_start) {
        print json_encode([
            'error' => "Activity date is outside the allowed range (" . $now->format($date_format) . " < " . $act_start->format($date_format) . ")"
        ]);
        wp_die();
    } elseif (!is_null($act_end) && $now > $act_end) {
        print json_encode([
            'error' => "Activity date is outside the allowed range (" . $now->format($date_format) . " > " . $act_end->format($date_format) . ")"
        ]);
        wp_die();
    }

    $user_id = null;

    if ($req_login && (! is_user_logged_in() || ! Point_Tracker_Pro::is_user_in_challenge($chal->id, get_current_user_id()))) {
        print json_encode([
            'error' => 'You must be logged in to access this (access denied)'
        ]);
        wp_die();
    } // if the user is logged in, get their info
    elseif (is_user_logged_in()) {
        $user_id = get_current_user_id();
    } // if login is not required and they aren't logged in, check for the presents of an account using their email
    elseif (! $req_login && ! $user_id) {
        $user_id = email_exists($user_email);
        if (! $user_id) {
            $random_pwd = wp_generate_password();
            $user_id = wp_create_user($user_email, $random_pwd, $user_email);
        }
    }

    if (! $user_id) {
        print json_encode([
            'error' => 'Unable to add you to the challenge',
            'uid' => $user_id
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT *
FROM {$wpdb->prefix}pt_participants
WHERE
    user_id = %d AND
    challenge_id = %d", $user_id, $chal->id);
    $part = $wpdb->get_row($query);
    $res = true;

    if (! $part) {
        $res = $wpdb->insert("{$wpdb->prefix}pt_participants", [
            'challenge_id' => $chal->id,
            'user_id' => $user_id,
            'leader_id' => $leader_id,
            'approved' => 1,
            'date_joined' => $now->format("Y-m-d"),
            'date_approved' => $now->format("Y-m-d"),
            'name' => $user_name,
            'email' => $user_email,
            'member_id' => $member_id
        ]);
    } else {
        $res = $wpdb->update("{$wpdb->prefix}pt_participants", [
            'leader_id' => $leader_id,
            'name' => $user_name,
            'email' => $user_email,
            'member_id' => $member_id
        ], [
            'challenge_id' => $chal->id,
            'user_id' => $user_id
        ]);
    }

    // Make sure a user was added to the challenge
    if ($res === false) {
        print json_encode([
            'error' => 'Unable to add you to the challenge',
            'res' => $res,
            'part' => $part
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_activities WHERE id = %d", $act_id);
    $act = $wpdb->get_row($query);

    if ($act->type == 'checkbox') {
        $value = implode(',', filter_input(INPUT_POST, 'value', FILTER_SANITIZE_STRING, FILTER_FORCE_ARRAY));
    } else {
        $value = sanitize_text_field(filter_input(INPUT_POST, 'value', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    }

    $params = [
        'user_id' => $user_id,
        'activity_id' => $act_id,
        'log_date' => $now->format("Y-m-d"),
        'log_time' => $now->format("H:i:s"),
        'value' => $value
    ];
    $amt = $act->points;

    // validate the activity by type
    switch ($act->type) {
        case 'checkbox':
            if (! strlen($value)) {
                print json_encode([
                    'warning' => 'Must select at least one option'
                ]);
                wp_die();
            }
            break;
        case 'number':
            if (empty($value) || ! is_numeric($value)) {
                print json_encode([
                    'warning' => 'Value must be numeric'
                ]);
                wp_die();
            } elseif ($act->min && $value < $act->min) {
                print json_encode([
                    'warning' => 'Value must be at least ' . $act->min
                ]);
                wp_die();
            } elseif ($act->max && $value > $act->max) {
                print json_encode([
                    'warning' => 'Value must be less than or equal to ' . $act->max
                ]);
                wp_die();
            } elseif ($value <= 0) {
                print json_encode([
                    'warning' => 'Value must be a positive integer'
                ]);
                wp_die();
            }
            $amt = ((int) $value) * ((int) $act->points);
            break;
        case 'radio':
            if (! strlen($value)) {
                print json_encode([
                    'warning' => 'Must select one option'
                ]);
                wp_die();
            }
            break;
        case 'text':
            if (empty($value)) {
                print json_encode([
                    'warning' => 'Cannot save an empty entry'
                ]);
                wp_die();
            } elseif ($act->min && strlen($value) < $act->min) {
                print json_encode([
                    'warning' => 'Text must be at least ' . $act->min . ' characters'
                ]);
                wp_die();
            } elseif ($act->max && strlen($value) > $act->max) {
                print json_encode([
                    'warning' => 'Text must be less than ' . $act->max . ' characters'
                ]);
                wp_die();
            }
    }

    // check to see if they have reached the maximum points allowed for that activity during the challenge
    if ($act->chal_max) {
        $wpdb->query($wpdb->prepare("SET @challenge_id=%d", $chal->id));
        $wpdb->query($wpdb->prepare("SET @activity_id=%d", $act->id));
        $query = $wpdb->prepare("SELECT COALESCE(SUM(total_points),0)
FROM {$wpdb->prefix}point_totals
WHERE user_id = %d", $user_id);
        $total_points = (int) $wpdb->get_var($query);

        // check to see if their current points + current value exceeds the maximum allowed
        if (($total_points + $amt) > $act->chal_max) {
            // check to see if the the maximum is still larger than what they have so we can add the difference from what they just entered
            if ($act->chal_max > $total_points) {
                $params['value'] = $act->chal_max - $total_points;
                $altered = true;
            } else {
                print json_encode([
                    'error' => 'You have reached the maximum points allowed for this activity for the duration of the challenge'
                ]);
                wp_die();
            }
        }
    }

    $res = $wpdb->insert("{$wpdb->prefix}pt_log", $params);

    print json_encode($res && $altered ? [
        'warning' => "You have reached the maximum points allowed for this activity ({$act->chal_max}) so your points were altered"
    ] : ($res ? [
        'success' => 'Activity was added'
    ] : [
        'error' => 'You have already recorded this activity for today'
    ]));

    wp_die();
}

/**
 * Function to get the user's activity
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_get_my_activity_table()
{
    global $wpdb;
    $_data = [];
    $tp = 0;
    $member_id = filter_input(INPUT_POST, 'member-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $email = strtolower(sanitize_email(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, FILTER_NULL_ON_FAILURE)));
    $chal_link = filter_input(INPUT_POST, 'chal-link', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE short_link = %s", $chal_link));
    if(!$chal) {
        print json_encode([
            'error' => 'Failed to retrieve challenge'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT user_id
FROM {$wpdb->prefix}pt_participants
WHERE
    challenge_id = %d AND
    member_id = %d AND
    email = %s", $chal->id, $member_id, $email);
    $uid = $wpdb->get_var($query);
    
    if (! $uid) {
        print json_encode([
            'error' => 'Failed to retrieve user id'
        ]);
    } else {
        $wpdb->query($wpdb->prepare("SET @challenge_id=%d", $chal->id));
        $tp = $wpdb->get_var($wpdb->prepare("SELECT SUM(total_points) FROM {$wpdb->prefix}leader_board WHERE user_id = %d", $uid));

        $query = $wpdb->prepare("SELECT
    ca.*,al.*
FROM {$wpdb->prefix}pt_challenges c
JOIN {$wpdb->prefix}pt_activities ca ON ca.`challenge_id` = c.id
JOIN {$wpdb->prefix}pt_log al ON al.`activity_id` = ca.id
WHERE
    al.`user_id` = %d AND
    c.id = %d
ORDER BY
    al.log_date,al.log_time", $uid, $chal->id);

        $my_act = $wpdb->get_results($query);

        foreach ($my_act as $act) {
            $dt = new DateTime("{$act->log_date} {$act->log_time}", new DateTimeZone(get_option('timezone_string')));
            $_data[] = [
                'name' => html_entity_decode($act->question, ENT_QUOTES | ENT_HTML5),
                'points' => $act->points,
                'date' => $dt->format(get_option('date_format', 'Y-m-d')),
                'time' => $dt->format(get_option('time_format', 'H:i:s')),
                'answer' => html_entity_decode($act->value, ENT_QUOTES | ENT_HTML5),
                'action' => "<i class='far fa-trash-alt' title='Delete this activity so you can input the correct info' data-act-id='{$act->id}' data-log-date='{$act->log_date}' data-user-id='{$act->user_id}'></i>"
            ];
        }

        print json_encode([
            'total_points' => $tp,
            'columns' => [
                [
                    'title' => 'Name',
                    'defaultContent' => '',
                    'data' => 'name'
                ],
                [
                    'title' => 'Points',
                    'defaultContent' => 0,
                    'data' => 'points'
                ],
                [
                    'title' => 'Date',
                    'defaultContent' => '',
                    'data' => 'date'
                ],
                [
                    'title' => 'Time',
                    'defaultContent' => '',
                    'data' => 'time'
                ],
                [
                    'title' => 'Answer',
                    'defaultContent' => '',
                    'data' => 'answer'
                ],
                [
                    'title' => 'Action',
                    'defaultContent' => '',
                    'data' => 'action'
                ]
            ],
            'data' => $_data
        ]);
    }

    wp_die();
}

/**
 * Function to delete the participants activity
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the operation
 */
function ptp_delete_participant_activity()
{
    global $wpdb;

    if (! check_ajax_referer('ptp-delete-entry', 'security', false)) {
        print json_encode([
            'error' => 'We were unable to verify the nonce'
        ]);
        wp_die();
    }

    $user_id = filter_input(INPUT_POST, 'user-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $act_id = filter_input(INPUT_POST, 'act-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $log_date = filter_input(INPUT_POST, 'log-date', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

    $dt = new DateTime($log_date, new DateTimeZone(get_option('timezone_string')));
    if (! is_a($dt, 'DateTime')) {
        print json_encode([
            'error' => 'Failed to determine the log date'
        ]);
        wp_die();
    }

    $res = $wpdb->delete("{$wpdb->prefix}pt_log", [
        'user_id' => $user_id,
        'activity_id' => $act_id,
        'log_date' => $dt->format("Y-m-d")
    ]);

    print json_encode($res ? [
        'success' => 'Activity was removed'
    ] : [
        'error' => "There was an error removing that activity, please contact the site admin " . get_option('admin-email', null)
    ]);

    wp_die();
}

/**
 * Function to get the leader board
 */
function ptp_get_leader_board()
{
    print json_encode([
        'lb' => ptp_get_leader_board_table(),
        'activities' => ptp_get_activity_leader_board_table()
    ]);
    wp_die();
}

/**
 * Function to get the leader board for a challenge
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string showing the leader board
 */
function ptp_get_leader_board_table()
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));

    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    $wpdb->query($wpdb->prepare("SET @challenge_id=%d", $chal_id));

    $lb = $wpdb->get_results("SELECT user_id,participant_name,SUM(total_points) as 'total',member_id,leader_id
FROM {$wpdb->prefix}leader_board
GROUP BY participant_name
ORDER BY `total` DESC");
    $_data = [];
    $leader = null;

    foreach ($lb as $l) {
        if ($chal->use_leader) {
            $leader = $wpdb->get_var($wpdb->prepare("SELECT leader_name
FROM {$wpdb->prefix}pt_leader_list
WHERE
    leader_id = %d AND
    challenge_id = %d", $l->leader_id, $chal_id));
        }

        $_data[] = [
            'id' => $l->member_id,
            'name' => $l->participant_name,
            'points' => $l->total,
            'leader' => $leader
        ];
    }

    $columns = [
        [
            'title' => 'ID',
            'defaultContent' => '',
            'data' => 'id'
        ],
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'name'
        ],
        [
            'title' => 'Points',
            'defaultContent' => 0,
            'data' => 'points'
        ]
    ];

    if ($leader) {
        $columns[] = [
            'title' => 'Leader',
            'defaultContent' => '',
            'data' => 'leader'
        ];
    }

    return [
        'data' => $_data,
        'columns' => $columns
    ];
}

/**
 * Function to retrieve the high scores for each of the activities
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string showing the activity leaders
 */
function ptp_get_activity_leader_board_table()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $activities = ptp_get_all_activities($chal_id);
    $_data = [];
    $wpdb->query("SET sql_mode = ''");

    foreach ($activities as $act) {
        $wpdb->query($wpdb->prepare("SET @challenge_id=%d", $chal_id));
        $query = $wpdb->prepare("SELECT question,participant_name,SUM(total_points) as 'high_score'
FROM {$wpdb->prefix}leader_board
WHERE activity_id = %d
GROUP BY participant_name
ORDER BY `high_score` DESC, log_date
LIMIT 1", $act->id);

        $hs = $wpdb->get_row($query);

        if ($hs) {
            $_data[] = [
                'activity' => html_entity_decode($hs->question, ENT_QUOTES | ENT_HTML5),
                'name' => html_entity_decode($hs->participant_name, ENT_QUOTES | ENT_HTML5),
                'hs' => $hs->high_score
            ];
        }
    }

    return [
        'data' => $_data,
        'columns' => [
            [
                'title' => 'Activity',
                'defaultContent' => '',
                'data' => 'activity'
            ],
            [
                'title' => 'Name',
                'defaultContent' => '',
                'data' => 'name'
            ],
            [
                'title' => 'High Score',
                'defaultContent' => 0,
                'data' => 'hs'
            ]
        ]
    ];
}

/**
 * Function to get the leader's log table
 */
function ptp_get_leader_log_table()
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the challenge'
        ]);
        wp_die();
    } elseif (! $chal->use_leader) {
        print json_encode([
            'error' => 'Challenge does not use a leader list'
        ]);
        wp_die();
    }

    $member_id = filter_input(INPUT_POST, 'leader-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $email = strtolower(sanitize_email(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, FILTER_NULL_ON_FAILURE)));
    $query = $wpdb->prepare("SELECT leader_id FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d AND leader_member_id = %d AND leader_email = %s", $chal_id, $member_id, $email);
    $leader_id = $wpdb->get_var($query);

    $query = $wpdb->prepare("SELECT p.challenge_id,p.user_id,p.member_id,p.name,l.activity_id,l.log_date,l.log_time,l.value,a.question,a.points
FROM {$wpdb->prefix}pt_participants p
JOIN {$wpdb->prefix}pt_log l ON l.user_id = p.user_id
JOIN {$wpdb->prefix}pt_activities a ON l.activity_id = a.id
WHERE
    p.leader_id = %d AND
    p.challenge_id = %d", $leader_id, $chal_id);
    $log_res = $wpdb->get_results($query);
    $_data = [];

    foreach ($log_res as $l) {
        $dt = new DateTime("{$l->log_date} {$l->log_time}", new DateTimeZone(get_option('timezone_string')));
        $_data[] = [
            'user_id' => $l->user_id,
            'member_id' => $l->member_id,
            'name' => $l->name,
            'activity_id' => $l->activity_id,
            'log_date' => $dt->format(get_option('date_format') . " " . get_option('time_format')),
            'value' => $l->value,
            'question' => $l->question,
            'points' => $l->points
        ];
    }

    $_columns = [
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'name'
        ],
        [
            'title' => 'ID',
            'defaultContent' => '',
            'data' => 'member_id'
        ],
        [
            'title' => 'Question',
            'defaultContent' => '',
            'data' => 'question'
        ],
        [
            'title' => 'Points',
            'defaultContent' => '',
            'data' => 'points'
        ],
        [
            'title' => 'Date/Time',
            'defaultContent' => '',
            'data' => 'log_date'
        ],
        [
            'title' => 'Answer',
            'defaultContent' => '',
            'data' => 'value'
        ]
    ];

    print json_encode([
        'data' => $_data,
        'columns' => $_columns
    ]);
    wp_die();
}

/**
 * Method to get users info when selecting a random winner
 * 
 * @since 1.2
 */
function ptp_get_random_selector_data()
{
    global $wpdb;
    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }
    
    $chal_id = filter_input(INPUT_POST, 'chal', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $act_id = filter_input(INPUT_POST, 'activity', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $leader_id = filter_input(INPUT_POST, 'leader', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $point_min = filter_input(INPUT_POST, 'point-min', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    
    $chal = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = $chal_id");
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }
    
    $activities = ptp_get_all_activities($chal_id);
    $leaders = ptp_get_challenge_leaders($chal_id);
    
    $columns = [
        [
            'title' => 'ID',
            'defaultContent' => '',
            'data' => 'id'
        ],
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'name'
        ],
        [
            'title' => 'Points',
            'defaultContent' => '',
            'data' => 'points'
        ]
    ];
    
    $query = $wpdb->prepare("CREATE TEMPORARY TABLE tmp_log SELECT
al.activity_id,al.log_date,al.log_time,al.value,ca.question,ca.points,cp.*
FROM {$wpdb->prefix}pt_log al
JOIN {$wpdb->prefix}pt_activities ca ON ca.id = al.activity_id
JOIN {$wpdb->prefix}pt_participants cp ON cp.user_id = al.user_id AND cp.challenge_id = ca.challenge_id
WHERE ca.challenge_id = %d", $chal_id);
    $wpdb->query($query);
    
    $leader_join = null;
    if($chal->use_leader) {
        $leader_join = "LEFT JOIN {$wpdb->prefix}pt_leader_list ll ON ll.leader_id = al.leader_id AND ll.challenge_id = al.challenge_id";
    }
    
    $where_sql = null;
    if($act_id && $leader_id) {
        $where_sql = "WHERE al.activity_id = {$wpdb->_escape($act_id)} AND ll.leader_id = {$wpdb->_escape($leader_id)}";
    }
    elseif($act_id) {
        $where_sql = "WHERE al.activity_id = {$wpdb->_escape($act_id)}";
    }
    elseif($leader_id) {
        $where_sql = "WHERE ll.leader_id = {$wpdb->_escape($leader_id)}";
    }
    
    $having_sql = null;
    if($point_min) {
        $having_sql = "HAVING `points` >= {$wpdb->_escape($point_min)}";
    }
    
    $query = "SELECT al.member_id, al.name, al.question, SUM(al.points) AS 'points', al.value, al.activity_id, al.user_id
FROM tmp_log al
$leader_join
$where_sql
GROUP BY al.member_id
$having_sql
ORDER BY al.log_date,al.log_time";
    $log_res = $wpdb->get_results($query);
    
    $_data = [];
    
    if (is_array($log_res) && count($log_res)) {
        foreach ($log_res as $log) {
            //$dt = new DateTime($log->log_date . " " . $log->log_time, new DateTimeZone(get_option('timezone_string')));
            $_data[] = [
                'id' => $log->member_id,
                'name' => html_entity_decode($log->name, ENT_QUOTES | ENT_HTML5),
                'points' => $log->points,
            ];
        }
    }
    
    print json_encode(['activities' => $activities, 'data' => $_data, 'columns' => $columns, 'leaders' => $leaders]);
        
    wp_die();
}

/**
 * Method to contact Random.org to retrieve a random number
 */
function ptp_select_random_winner()
{
    global $wpdb;
    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    } elseif(!($apiKey = get_option('ptp-random-api-key', null))) {
        print json_encode([
            'error' => 'You have not save an API key for Random.org Settings -> Point Tracker'
        ]);
        wp_die();
    }
    
    $chal_id = filter_input(INPUT_POST, 'chal', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $name_list = filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    $ppe = (boolean) filter_input(INPUT_POST, 'entry', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    
    $chal = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = $chal_id");
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }
    
    $names = [];
    $ids = [];
    if(is_array($name_list) && count($name_list)) {
        foreach($name_list as $name) {
            $ids[$name['name']] = $name['id'];
            if($ppe) {
                $names = array_merge($names, array_fill(count($names), $name['points'], $name['name']));
            } else {
                $names[] = $name['name'];
            }
        }
    }
    
    if(count($names) < 2) {
        print json_encode([
            'error' => 'You must have at least 2 entries'
        ]);
        wp_die();
    }
    
    shuffle($names);
    
    $client = new Client("https://api.random.org/json-rpc/1/invoke");
    $client = $client->query(1, 'generateIntegers', [
        'apiKey' => $apiKey,
        'n' => 1,
        'min' => 0,
        'max' => count($names) - 1,
        'replacement' => false
    ]);
    /** @var Datto\JsonRpc\Response $reply */
    $reply = $client->send();
    
    if(isset($reply[0])) {
        $reply = $reply[0];
    }
    
    if (!$reply->getResult()) {
        print json_encode(['error' => $reply->getError()->getMessage(), 'reply' => $reply]);
    }
    else {
        $index = $reply->getResult()['random']['data'][0];
        $id = $ids[$names[$index]];
        
        $user_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d AND member_id = %d", $chal_id, $id));
        
        if($user_id) {
            $wpdb->update("{$wpdb->prefix}pt_challenges", ['winner' => $user_id], ['id' => $chal_id]);
        }
        
        print json_encode(['winner' => $names[$index], 'number' => $index, 'total' => count($names)]);
    }
    
    update_option('ptp-random-requests', $reply->getResult()['requestsLeft']);
    update_option('ptp-random-bits', $reply->getResult()['bitsLeft']);
    
    wp_die();
}
