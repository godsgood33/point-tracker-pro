<?php
add_action('wp_ajax_add-leader', 'ptp_add_leader');
add_action('wp_ajax_remove-leader', 'ptp_remove_leader');
add_action('wp_ajax_email-participant-list', 'ptp_email_participant_list');
add_action('wp_ajax_email-participant-log', 'ptp_email_participant_log');
add_action('wp_ajax_get-leader-board', 'ptp_get_leader_board_by_leader');
add_action('phpmailer_init', 'ptp_phpmailer_init');
add_action('wp_mail_failed', 'ptp_phpmailer_error');

add_action('wp_ajax_nopriv_get-leader-board', 'ptp_get_leader_board_by_leader');

/**
 * Function to add a leader to the challenge
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_add_leader()
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal_id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $member_id = filter_input(INPUT_POST, 'member_id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $name = sanitize_text_field(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    $email = strtolower(sanitize_email(filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE)));

    if (! $email) {
        print json_encode([
            'error' => 'That is an invalid email'
        ]);
        wp_die();
    }

    $user_id = email_exists($email);

    if (! $user_id) {
        $random_pwd = wp_generate_password();
        $user_id = wp_create_user($email, $random_pwd, $email);
    }

    if (! $user_id) {
        print json_encode([
            'error' => 'Unable to create the user and add as a leader'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d AND leader_id = %d", $chal_id, $user_id);
    $count = $wpdb->get_var($query);
    $res = true;

    if (! $count) {
        $res = $wpdb->insert("{$wpdb->prefix}pt_leader_list", [
            'challenge_id' => $chal_id,
            'leader_id' => $user_id,
            'leader_name' => $name,
            'leader_email' => $email,
            'leader_member_id' => $member_id
        ]);
    }

    print json_encode($res !== false ? [
        'success' => 'Added leader',
        'name' => $name,
        'email' => $email,
        'leader_id' => $user_id
    ] : [
        'error' => $wpdb->last_error
    ]);
    wp_die();
}

/**
 * Function to remove a leader from the challenge
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_remove_leader()
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $leader_id = filter_input(INPUT_POST, 'leader-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    if (! $chal_id || ! $leader_id) {
        print json_encode([
            'error' => 'Missing challenge or leader ID'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_leader_list WHERE leader_id = %d AND challenge_id = %d", $leader_id, $chal_id);
    $count = $wpdb->get_var($query);

    if ($count) {
        $wpdb->update("{$wpdb->prefix}pt_participants", [
            'leader_id' => null
        ], [
            'leader_id' => $leader_id,
            'challenge_id' => $chal_id
        ]);

        $res = $wpdb->delete("{$wpdb->prefix}pt_leader_list", [
            'leader_id' => $leader_id,
            'challenge_id' => $chal_id
        ]);
    }

    print json_encode($res !== false ? [
        'success' => 'Deleted leader from leader list'
    ] : [
        'error' => $wpdb->last_error
    ]);
    wp_die();
}

/**
 * Function to email challenge leaders their participant list
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_email_participant_list()
{
    set_time_limit(0);
    global $wpdb;
    $dt = new DateTime("now", new DateTimeZone(get_option("timezone_string")));

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the challenge selected'
        ]);
        wp_die();
    } elseif (! $chal->use_leader) {
        print json_encode([
            'error' => 'Challenge is not using a leader list'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d", $chal_id);
    $leaders = $wpdb->get_results($query);

    foreach ($leaders as $l) {
        $fname = get_temp_dir() . "{$chal->name}-{$l->leader_name}-ParticipantList-{$dt->format("Y-m-d")}.csv";
        ptp_write_participant_list($fname, $l->leader_id);
        $msg = null;
        $res = false;
        try {
            $res = wp_mail($l->leader_email, "{$chal->name} Participant List", "Attached is the participant list for the challenge", null, [
                $fname
            ]);
        } catch(Exception $er) {
            $msg = $er->getMessage();
        }

        if ($res !== true) {
            print json_encode([
                'error' => $msg,
                'fname' => $fname
            ]);
            wp_die();
        } else {
            unlink($fname);
        }
    }

    print json_encode([
        'success' => 'Reports emailed to all leaders'
    ]);
    wp_die();
}

/**
 * Function to email challenge leaders their participant log
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_email_participant_log()
{
    set_time_limit(0);
    global $wpdb;
    $dt = new DateTime("now", new DateTimeZone(get_option('timezone_string')));

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    if (! $chal) {
        print json_encode([
            'error' => "Failed to find the challenge"
        ]);
        wp_die();
    } elseif (! $chal->use_leader) {
        print json_encode([
            'error' => 'Challenge is not using a leader list'
        ]);
        wp_die();
    }

    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d", $chal_id);
    $leaders = $wpdb->get_results($query);

    foreach ($leaders as $l) {
        $fname = get_temp_dir() . "{$chal->name}-{$l->leader_name}-ParticipantLog-{$dt->format("Y-m-d")}.csv";
        ptp_write_participant_log($fname, $l->leader_id);
        $msg = null;
        $res = false;
        try {
            $res = wp_mail($l->leader_email, "{$chal->name} Participant Log", "Attached is the activity log for participants assigned to you in the challenge", null, [
                $fname
            ]);
        } catch (Exception $er) {
            $msg = $er->getMessage();
        }

        if ($res !== true) {
            print json_encode([
                'error' => $msg,
                'fname' => $fname
            ]);
            wp_die();
        } else {
            unlink($fname);
        }
    }

    print json_encode([
        'success' => 'Reports emailed to all leaders'
    ]);
    wp_die();
}

/**
 * Function to get the leader board by leader
 *
 * @return string
 */
function ptp_get_leader_board_by_leader()
{
    global $wpdb;
    $ret = [
        'lb' => [],
        'act' => []
    ];

    $chal_link = filter_input(INPUT_POST, 'chal-link', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE short_link = %s", $chal_link));
    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the selected challenge'
        ]);
        wp_die();
    }

    $wpdb->query($wpdb->prepare("SET @challenge_id = %d", $chal->id));
    $wpdb->query("SET sql_mode=''");

    $leader = filter_input(INPUT_POST, 'leader', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $lb = $wpdb->get_results($wpdb->prepare("SELECT participant_name,SUM(total_points) AS 'total'
FROM {$wpdb->prefix}leader_board
WHERE leader_id = %d
GROUP BY participant_name
ORDER BY `total` DESC" . (is_numeric($chal->leader_count) ? " LIMIT {$chal->leader_count}" : ""), $leader));
    foreach ($lb as $l) {
        $ret['lb'][] = [
            'name' => $l->participant_name,
            'pts' => $l->total
        ];
    }

    $activities = ptp_get_all_activities($chal->id);

    foreach ($activities as $act) {
        $query = $wpdb->prepare("SELECT question,participant_name,SUM(total_points) as 'high_score'
FROM {$wpdb->prefix}leader_board
WHERE
    activity_id = %d AND
    leader_id = %d
GROUP BY participant_name
ORDER BY `high_score` DESC, log_date
LIMIT 1", $act->id, $leader);
        $hs = $wpdb->get_row($query);

        if ($hs) {
            $ret['act'][] = [
                'question' => $hs->question,
                'name' => $hs->participant_name,
                'high_score' => $hs->high_score
            ];
        }
    }

    print json_encode($ret);

    wp_die();
}

/**
 * Function to get all the leader info
 * 
 * @global wpdb $wpdb
 * 
 * @param int $chal_id
 * 
 * @return array
 */
function ptp_get_challenge_leaders($chal_id)
{
    global $wpdb;
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_leader_list WHERE challenge_id = %d", $chal_id);
    $results = $wpdb->get_results($query);
    
    return $results;
}

/**
 * Function to initialize the
 *
 * @param PHPMailer $phpmailer
 */
function ptp_phpmailer_init(PHPMailer $phpmailer)
{
    if(empty(get_option('ptp-mailserver-host')) || preg_match('/example/', get_option('ptp-mailserver-host', ''))) {
        throw new WP_Error("Need to setup email");
    }

    $phpmailer->Host = get_option('ptp-mailserver-host');
    $phpmailer->SMTPAuth = (bool) get_option('ptp-mailserver-require-smtp-auth', 0);
    $phpmailer->Port = get_option('ptp-mailserver-port');
    $phpmailer->Username = get_option('ptp-mailserver-user');
    $phpmailer->Password = get_option('ptp-mailserver-pwd');
}

/**
 * Method to catch errors if fail to send email
 * 
 * @param WP_Error $er
 *
 * @throws Exception
 */
function ptp_phpmailer_error(WP_Error $er)
{
    if(isset($er->errors) && isset($er->errors['wp_mail_failed']) && isset($er->errors['wp_mail_failed'][0])) {
        throw new Exception($er->errors['wp_mail_failed'][0], E_ERROR);
    }
}
