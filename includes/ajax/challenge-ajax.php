<?php
add_action('wp_ajax_get-challenge', 'ptp_get_challenge');
add_action('wp_ajax_save-challenge', 'ptp_save_challenge');
add_action('wp_ajax_delete-challenge', 'ptp_delete_challenge');
add_action('wp_ajax_get-widget-data', 'ptp_get_widget_data');
add_action('wp_ajax_create-page', 'ptp_create_challenge_page');
add_action('wp_ajax_remove-winner', 'ptp_remove_winner');

/**
 * Getter function for the challenge specifics
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string of challenge data
 */
function ptp_get_challenge()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Error retrieving challenge (access denied)'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id);
    $chal = $wpdb->get_row($query);

    if (! $chal) {
        print json_encode([
            'error' => 'Unable to find the challenge selected'
        ]);
        wp_die();
    }

    $start = new DateTime($chal->start);
    $end = new DateTime($chal->end);

    $chal->name = html_entity_decode($chal->name, ENT_QUOTES | ENT_HTML5);
    $chal->start = $start->format(get_option('date_format', 'm/d/Y'));
    $chal->end = $end->format(get_option('date_format', 'm/d/Y'));
    $chal->desc = html_entity_decode(stripcslashes($chal->desc), ENT_QUOTES | ENT_HTML5);
    $chal->act_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_activities WHERE challenge_id = %d", $chal_id));
    $chal->part_count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d", $chal_id));
    
    if(is_numeric($chal->winner)) {
        $chal->winner = $wpdb->get_var($wpdb->prepare("SELECT name FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d AND user_id = %d", $chal_id, $chal->winner));
    }
    
    $query = $wpdb->prepare("SELECT ll.leader_member_id,ll.leader_name,ll.leader_email,ll.leader_id,(
    SELECT COUNT(1) FROM {$wpdb->prefix}pt_participants cp WHERE cp.leader_id = ll.leader_id AND cp.challenge_id = ll.challenge_id
) AS 'part_count'
FROM {$wpdb->prefix}pt_leader_list ll
WHERE ll.challenge_id = %d", $chal_id);
    $ll = $wpdb->get_results($query);

    foreach ($ll as $k => $l) {
        $ll[$k]->remove = "<i class='far fa-trash-alt' title='Delete this leader and remove any participant assignments' data-leader-id='{$l->leader_id}'></i>";
    }

    $chal->leader_list->data = $ll;
    $chal->leader_list->columns = [
        [
            'title' => 'ID',
            'defaultContent' => '',
            'data' => 'leader_member_id'
        ],
        [
            'title' => 'Name',
            'defaultContent' => '',
            'data' => 'leader_name'
        ],
        [
            'title' => 'Email',
            'defaultContent' => '',
            'data' => 'leader_email'
        ],
        [
            'title' => 'Count',
            'defaultContent' => '',
            'data' => 'part_count'
        ],
        [
            'title' => 'Remove',
            'defaultContent' => '',
            'data' => 'remove'
        ]
    ];
    
    $title = sanitize_title($chal->name);
    $page = get_page_by_title($chal->name);
    $chal->has_page = false;
    $chal->page_title = $title;
    if($page->ID) {
        $chal->has_page = true;
        $chal->guid = $page->guid;
    }

    print json_encode($chal);

    wp_die();
}

/**
 * Function to save a challenge
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the success of the save operation
 */
function ptp_save_challenge()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You are not an admin (access denied)'
        ]);
        wp_die();
    }
    $req_start_date = filter_input(INPUT_POST, 'start-date', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
    $req_end_date = filter_input(INPUT_POST, 'end-date', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);

    $start_dt = new DateTime($req_start_date, new DateTimeZone(get_option('timezone_string')));
    if(!is_a($start_dt, 'DateTime')) {
        print json_encode([
            'error' => 'Not a valid start date'
        ]);
        wp_die();
    }
    $end_dt = new DateTime($req_end_date, new DateTimeZone(get_option('timezone_string')));
    if(!is_a($end_dt, 'DateTime')) {
        print json_encode([
            'error' => 'Not a valid end date'
        ]);
        wp_die();
    }
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $name = sanitize_text_field(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE));
    $lc = filter_input(INPUT_POST, 'leader-count', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE) or 'all';

    $params = [
        'name' => $name,
        'start' => $start_dt->format("Y-m-d"),
        'end' => $end_dt->format("Y-m-d"),
        'approval' => (boolean) filter_input(INPUT_POST, 'approval', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        'desc' => sanitize_textarea_field(filter_input(INPUT_POST, 'desc', FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE)),
        'leader_board' => (boolean) filter_input(INPUT_POST, 'leader-board', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        'leader_count' => $lc,
        'backdating' => (boolean) filter_input(INPUT_POST, 'backdating', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        'use_leader' => (boolean) filter_input(INPUT_POST, 'use-leader', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        'private' => (boolean) filter_input(INPUT_POST, 'private', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
    ];
    if ($chal_id) {
        $res = $wpdb->update("{$wpdb->prefix}pt_challenges", $params, [
            'id' => $chal_id
        ]);

        if (! $params['approval']) {
            $wpdb->update("{$wpdb->prefix}pt_participants", [
                "approved" => 1
            ], [
                'challenge_id' => $chal_id
            ]);
        }

        if (! $params['use_leader']) {
            $wpdb->delete("{$wpdb->prefix}pt_leader_list", [
                'challenge_id' => $chal_id
            ]);
        }
    } else {
        $params['short_link'] = uniqid();

        $res = $wpdb->insert("{$wpdb->prefix}pt_challenges", $params);
        if ($res) {
            $chal_id = $wpdb->insert_id;
        }
    }

    $query = $wpdb->prepare("SELECT short_link FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id);
    $link = isset($params['short_link']) ? $params['short_link'] : $wpdb->get_var($query);
    $slug = sanitize_title($name);
    $page = get_page_by_title($slug);
    $dt = new DateTime("now", new DateTimeZone(get_option('timezone_string')));
    $gmt = new DateTime("now", new DateTimeZone("UTC"));
    
    if(!$page) {
        $post = [
            'post_title' => $name,
            'post_author' => get_current_user_id(),
            'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge chal='{$link}'][/challenge]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
            'post_type' => 'page',
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_date' => $dt->format("Y-m-d H:i:s"),
            'post_date_gmt' => $gmt->format("Y-m-d H:i:s")
        ];
        
        $page_id = wp_insert_post($post);
        
        if(is_a($page_id, 'WP_Error')) {
            print json_encode(['error' => 'There was an error creating the page for the challenge']);
            wp_die();
        }
        
        $guid = get_permalink($page_id);
    } else {
        $page_id = $page->ID;
        $guid = $page->guid;
    }

    print json_encode($res === false ? [
        'error' => 'Update failed'
    ] : [
        'success' => 'Challenge Saved',
        'uid' => $link,
        'id' => $chal_id,
        'name' => $name,
        'slug' => $slug,
        'url' => $guid,
        'page_id' => $page_id,
        'page' => $post
    ]);

    wp_die();
}

/**
 * Method to create a page just for the challenge
 */
function ptp_create_challenge_page()
{
    global $wpdb;
    
    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You are not an admin (access denied)'
        ]);
        wp_die();
    }
    
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id);
    $chal = $wpdb->get_row($query);
    
    $slug = sanitize_title($chal->name);
    $page = get_page_by_title($slug);
    $dt = new DateTime("now", new DateTimeZone(get_option('timezone_string')));
    $gmt = new DateTime("now", new DateTimeZone("UTC"));
    
    if(is_null($page)) {
        $post = [
            'post_title' => $chal->name,
            'post_author' => get_current_user_id(),
            'post_content' => (Point_Tracker_Pro::gutenberg_installed() ? "<!-- wp:shortcode --> " : "") . "[challenge chal='{$chal->short_link}'][/challenge]" . (Point_Tracker_Pro::gutenberg_installed() ? " <!-- /wp:shortcode -->" : ""),
            'post_type' => 'page',
            'post_status' => 'publish',
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_date' => $dt->format("Y-m-d H:i:s"),
            'post_date_gmt' => $gmt->format("Y-m-d H:i:s")
        ];
        
        $page_id = wp_insert_post($post);
        
        if(is_a($page_id, 'WP_Error')) {
            print json_encode(['error' => "Failed to create page", 'page-data' => $post]);
            wp_die();
        }
        
        $guid = get_permalink($page_id);
        
        print json_encode([
            'success' => 'Created new page',
            'slug' => $slug,
            'url' => $guid,
            'id' => $page_id,
            'title' => $chal->name
        ]);
        wp_die();
    }
    
    print json_encode(['warning' => 'Page already exists']);
    wp_die();
}

/**
 * Function to delete a challenge
 *
 * @global wpdb $wpdb
 *
 * @return string JSON encoded string representing the status of the deletion operation
 */
function ptp_delete_challenge()
{
    global $wpdb;

    if (! current_user_can('manage_options')) {
        print json_encode([
            'error' => 'Access Denied'
        ]);
        wp_die();
    } elseif (! check_ajax_referer('ptp-delete-challenge', 'security', false)) {
        print json_encode([
            'error' => 'We were unable to verify the nonce'
        ]);
        wp_die();
    }

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

    $res = $wpdb->delete("{$wpdb->prefix}pt_challenges", [
        'id' => $chal_id
    ]);

	if($res) {
        $activities = $wpdb->get_results($wpdb->prepare("SELECT id FROM `{$wpdb->prefix}pt_activities` WHERE challenge_id=%d", $chal_id));

        $wpdb->delete("{$wpdb->prefix}pt_activities", [
            'challenge_id' => $chal_id
        ]);
        $wpdb->delete("{$wpdb->prefix}pt_participants", [
            'challenge_id' => $chal_id
        ]);

        foreach ($activities as $act) {
            $wpdb->delete("{$wpdb->prefix}pt_log", [
                "activity_id" => $act
            ]);
        }
    }

	print json_encode($res ? [
        'success' => 'Successfully deleted challenge'
    ] : [
		'error' => $wpdb->last_error
	]);

    wp_die();
}

/**
 * Function to retrieve the widget data
 *
 * @global wpdb $wpdb
 *
 * @return string
 */
function ptp_get_widget_data()
{
    global $wpdb;

    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $type = filter_input(INPUT_POST, 'report-type', FILTER_VALIDATE_REGEXP, ['options' => [
        'regexp' => "/participants|challenge|log|activities/"
    ], 'flags' => FILTER_NULL_ON_FAILURE]);
    $data = null;
    
    $date_format = get_option('date_format', 'Y-m-d');

    if($type == 'challenge') {
        $chal = get_page_by_title("Challenge");
        
        $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id);
        $rows = $wpdb->get_row($query);
        $start_date = new DateTime($rows->start);
        $end_date = new DateTime($rows->end);
        $data->short_link = $rows->short_link;
        $data->chal_url = $chal->guid;
        $data->start = $start_date->format($date_format);
        $data->end = $end_date->format($date_format);
        
        $query = $wpdb->prepare("SELECT COUNT(1) FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d", $chal_id);
        $data->p_count = $wpdb->get_var($query);
        
        $wpdb->query("SET @challenge_id = $chal_id");
        $query = "SELECT SUM(`total_points`) FROM {$wpdb->prefix}leader_board";
        $data->total_points = $wpdb->get_var($query);
        
        $wpdb->query("SET @challenge_id = $chal_id");
        $query = "SELECT participant_name, total_points FROM {$wpdb->prefix}leader_board ORDER BY total_points DESC LIMIT 1";
        $row = $wpdb->get_row($query);
        $data->leader = $row->participant_name;
        $data->leaders_points = $row->total_points;
    }
    elseif($type == 'activities') {
        $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_activities WHERE challenge_id = %d ORDER BY `order`", $chal_id);
        $rows = $wpdb->get_results($query);
        if(is_array($rows) && count($rows) && isset($rows[0])) {
            foreach($rows as $row) {
                $wpdb->query("SET @activity_id = {$row->id}");
                $pt = $wpdb->get_var("SELECT SUM(`total_points`) FROM {$wpdb->prefix}point_totals");
                $data[] = [
                    'group' => $row->group,
                    'name' => $row->name,
                    'points' => $row->points,
                    'pt' => $pt
                ];
            }
        } elseif(isset($rows->id)) {
            $wpdb->query("SET @activity_id = {$rows->id}");
            $pt = $wpdb->get_var("SELECT SUM(`total_points`) FROM {$wpdb->prefix}point_totals");
            $data[] = [
                'group' => $rows->group,
                'name' => $rows->name,
                'points' => $rows->points,
                'pt' => $pt
            ];
        }
    }
    elseif($type == 'participants') {
        $query = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_participants WHERE challenge_id = %d ORDER BY `name`", $chal_id);
        $rows = $wpdb->get_results($query);
        
        if(is_array($rows) && count($rows) && isset($rows[0])) {
            foreach($rows as $row) {
                $email = explode("@", $row->email);
                $wpdb->query("SET @challenge_id = $chal_id");
                $wpdb->query("SET @user_id = {$row->user_id}");
                $pt = $wpdb->get_var("SELECT SUM(`total_points`) FROM {$wpdb->prefix}user_activity");
                $data[] = [
                    'name' => $row->name,
                    'email' => "{$email[0]}<br />@{$email[1]}",
                    'full_email' => $row->email,
                    'approved' => (bool) $row->approved,
                    'pt' => $pt
                ];
            }
        } elseif(isset($rows->user_id)) {
            $email = explode("@", $rows->email);
            $wpdb->query("SET @challenge_id = $chal_id");
            $wpdb->query("SET @user_id = {$rows->user_id}");
            $pt = $wpdb->get_var("SELECT SUM(`total_points`) FROM {$wpdb->prefix}user_activity");
            $data[] = [
                'name' => $rows->name,
                'email' => "{$email[0]}<br />@{$email[1]}",
                'full_email' => $rows->email,
                'approved' => (bool) $rows->approved,
                'pt' => $pt
            ];
        }
    }
    elseif($type == 'log') {
    }

    print json_encode($data);
    wp_die();
}

/**
 * Function to remove a winner from a challenge
 *
 * @global wpdb $wpdb
 */
function ptp_remove_winner()
{
    global $wpdb;
    
    if(!current_user_can('manage_options')) {
        print json_encode([
            'error' => 'You do not have permissions to perform this action'
        ]);
        wp_die();
    }
    
    $chal_id = filter_input(INPUT_POST, 'chal-id', FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
    $chal = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}pt_challenges WHERE id = %d", $chal_id));
    
    if(!$chal) {
        print json_encode([
            'error' => 'Could not find the selected challenge'
        ]);
        wp_die();
    }
    
    if(!$wpdb->update("{$wpdb->prefix}pt_challenges", ['winner' => null], ['id' => $chal_id])) {
        print json_encode([
            'error' => 'There was an error removing the winner from the challenge',
            'msg' => $wpdb->last_error
        ]);
        wp_die();
    }
    
    print json_encode([
        'success' => 'Successfully removed the winner from the challenge'
    ]);
    wp_die();
}
